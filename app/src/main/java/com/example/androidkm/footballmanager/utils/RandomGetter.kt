package com.example.androidkm.footballmanager.utils

import com.example.androidkm.footballmanager.obj.PlayerBase
import com.example.androidkm.footballmanager.obj.TacticsPosition
import java.util.*

/**
 * Created by Karol on 2018-05-21.
 */
class RandomGetter {
    companion object {
        val random = Random()

        fun <T : Enum<*>> randomEnum(clazz: Class<T>): T {
            val x = random.nextInt(clazz.enumConstants.size)
            return clazz.enumConstants[x]
        }

        fun randomPlayer(positions: List<TacticsPosition>): TacticsPosition {
            val fieldPlayers = positions.filter { pos -> pos.fullPosition.pos != Position.GK && pos.player != null }
            val x = random.nextInt(fieldPlayers.size)
            return fieldPlayers[x]
        }
    }
}