package com.example.androidkm.footballmanager.team

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.androidkm.footballmanager.R

class TeamManagementActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_team_management)
    }

    fun playerStatusClick(view: View) {
        startActivity(Intent(this@TeamManagementActivity, TeamStatusActivity::class.java))
    }

    fun trainingClick(view: View) {
        startActivity(Intent(this@TeamManagementActivity, TeamTrainingActivity::class.java))
    }

    fun tacticsClick(view: View) {
        startActivity(Intent(this@TeamManagementActivity, TeamTacticsActivity::class.java))
    }

    fun transfersClick(view: View) {
        startActivity(Intent(this@TeamManagementActivity, TeamTransferActivity::class.java))

    }
}
