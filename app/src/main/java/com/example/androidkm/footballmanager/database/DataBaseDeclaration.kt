package com.example.androidkm.footballmanager.database

import android.arch.persistence.db.SupportSQLiteDatabase
import android.arch.persistence.room.*
import android.content.Context
import com.example.androidkm.footballmanager.utils.Position
import com.example.androidkm.footballmanager.utils.Side
import org.jetbrains.anko.doAsync

@Entity
data class DbPerson(
    @PrimaryKey(autoGenerate = true)
    val uid: Long,
    val firstName: String = "",
    val lastName: String = "",
    val beingUsed: Boolean = false
)

@Dao
interface DbPersonDao {

    @Query("SELECT * FROM dbperson")
    fun getAllPeople(): List<DbPerson>

    @Query("SELECT * FROM dbperson WHERE beingUsed = 0")
    fun getAllFreePeople(): List<DbPerson>

    @Query("UPDATE dbperson SET beingUsed = 1 WHERE uid = :id")
    fun setPersonAsUsed(id: Long)

    @Insert
    fun insertAll(people: List<DbPerson>)

    @Insert
    fun insert(person: DbPerson)

    @Update
    fun update(person: DbPerson)
}

@Entity
data class DbClub(
    @PrimaryKey(autoGenerate = true)
    val uid: Long,
    val name: String = ""
)

@Dao
interface DbClubDao {

    @Query("SELECT * FROM dbclub")
    fun getAllClubs(): List<DbClub>

    @Insert
    fun insertAll(clubs: List<DbClub>)

    @Insert
    fun insert(club: DbClub)
}

@Database(entities = [(DbPerson::class), (DbClub::class)], version = 2)
abstract class FootballDatabase : RoomDatabase() {

    abstract fun personDao(): DbPersonDao
    abstract fun clubDao(): DbClubDao

    //part of the code from https://gist.github.com/florina-muntenescu/697e543652b03d3d2a06703f5d6b44b5

    companion object {

        @Volatile
        private var INSTANCE: FootballDatabase? = null

        fun getInstance(context: Context): FootballDatabase =
                INSTANCE
                        ?: synchronized(this) {
                    INSTANCE
                            ?: buildDatabase(context).also { INSTANCE = it }
                }

        private fun buildDatabase(context: Context) =
                Room.databaseBuilder(context.applicationContext,
                        FootballDatabase::class.java, "Football.db")
                        .addCallback(object : Callback() {
                            override fun onCreate(db: SupportSQLiteDatabase) {
                                super.onCreate(db)

                                doAsync {
                                    getInstance(context).personDao().insertAll(preparePeople())
                                    getInstance(context).clubDao().insertAll(prepareClubs())
                                }
                            }
                        })
                        .build()

        private fun prepareClubs(): ArrayList<DbClub> {

            var array: ArrayList<DbClub> = ArrayList()

            array.add(DbClub(0, "FC Wrocław"))
            array.add(DbClub(0, "FC Bydgoszcz"))
            array.add(DbClub(0, "FC Lublin"))
            array.add(DbClub(0, "FC Gorzów Wielkopolski"))
            array.add(DbClub(0, "FC Łódź"))
            array.add(DbClub(0, "FC Kraków"))
            array.add(DbClub(0, "FC Warszawa"))
            array.add(DbClub(0, "FC Rzeszów"))
            array.add(DbClub(0, "FC Białystok"))
            array.add(DbClub(0, "FC Gdańsk"))
            array.add(DbClub(0, "FC Katowice"))
            array.add(DbClub(0, "FC Kielce"))
            array.add(DbClub(0, "FC Olsztyn"))
            array.add(DbClub(0, "FC Poznań"))
            array.add(DbClub(0, "FC Szczecin"))

            return array
        }

        private fun preparePeople(): ArrayList<DbPerson> {

            var array: ArrayList<DbPerson> = ArrayList()

            array.add(DbPerson(0, "Maciej", "Gorczyca"))
            array.add(DbPerson(0, "Karol", "Bieńko"))
            array.add(DbPerson(0, "Michał", "Rutkiewicz"))
            array.add(DbPerson(0, "Maksymilian", "Statkiewicz"))
            array.add(DbPerson(0, "Stanisław", "Goluch"))
            array.add(DbPerson(0, "Olaf", "Popiel"))
            array.add(DbPerson(0, "Antoni", "Kapuściński"))
            array.add(DbPerson(0, "Kajetan", "Ceglarek"))
            array.add(DbPerson(0, "Mateusz", "Madejski"))
            array.add(DbPerson(0, "Jakub", "Kustra"))
            array.add(DbPerson(0, "Jan", "Łaszkiewicz"))
            array.add(DbPerson(0, "Tomasz", "Komenda"))
            array.add(DbPerson(0, "Kamil", "Jaskuła"))
            array.add(DbPerson(0, "Wojciech", "Bukowski"))
            array.add(DbPerson(0, "Szymon", "Dobija"))
            array.add(DbPerson(0, "Krzysztof", "Kurpiewski"))
            array.add(DbPerson(0, "Kacper", "Turski"))
            array.add(DbPerson(0, "Mikołaj", "Zaczek"))
            array.add(DbPerson(0, "Sebastian", "Grzegorczyk"))
            array.add(DbPerson(0, "Eryk", "Kaptur"))
            array.add(DbPerson(0, "Bartłomiej", "Łagodziński"))
            array.add(DbPerson(0, "Adrian", "Sopel"))
            array.add(DbPerson(0, "Hubert", "Łuba"))
            array.add(DbPerson(0, "Aleksander", "Krasuski"))
            array.add(DbPerson(0, "Patryk", "Magnuszewski"))
            array.add(DbPerson(0, "Rafał", "Powroźnik"))
            array.add(DbPerson(0, "Jacek", "Masłowski"))
            array.add(DbPerson(0, "Bartosz", "Sobol"))
            array.add(DbPerson(0, "Filip", "Paśnik"))
            array.add(DbPerson(0, "Dominik", "Ziemba"))
            array.add(DbPerson(0, "Jarosław", "Wojas"))
            array.add(DbPerson(0, "Karol", "Hojka"))
            array.add(DbPerson(0, "Michał", "Szczęch"))
            array.add(DbPerson(0, "Maksymilian", "Ciszewski"))
            array.add(DbPerson(0, "Stanisław", "Majkut"))
            array.add(DbPerson(0, "Olaf", "Parys"))
            array.add(DbPerson(0, "Antoni", "Dziekoński"))
            array.add(DbPerson(0, "Kajetan", "Szlachetka"))
            array.add(DbPerson(0, "Mateusz", "Łyczko"))
            array.add(DbPerson(0, "Jakub", "Polek"))
            array.add(DbPerson(0, "Jan", "Szczech"))
            array.add(DbPerson(0, "Tomasz", "Stachoń"))
            array.add(DbPerson(0, "Kamil", "Dudek"))
            array.add(DbPerson(0, "Wojciech", "Dyga"))
            array.add(DbPerson(0, "Szymon", "Oniszczuk"))
            array.add(DbPerson(0, "Krzysztof", "Rolka"))
            array.add(DbPerson(0, "Kacper", "Klimczyk"))
            array.add(DbPerson(0, "Mikołaj", "Błaszak"))
            array.add(DbPerson(0, "Sebastian", "Jabłonowski"))
            array.add(DbPerson(0, "Eryk", "Chorąży"))
            array.add(DbPerson(0, "Bartłomiej", "Łapa"))
            array.add(DbPerson(0, "Adrian", "Suliński"))
            array.add(DbPerson(0, "Hubert", "Steć"))
            array.add(DbPerson(0, "Aleksander", "Rabiega"))
            array.add(DbPerson(0, "Patryk", "Wojcieszek"))
            array.add(DbPerson(0, "Rafał", "Dudziński"))
            array.add(DbPerson(0, "Jacek", "Godyń"))
            array.add(DbPerson(0, "Bartosz", "Korzec"))
            array.add(DbPerson(0, "Filip", "Dombrowski"))
            array.add(DbPerson(0, "Dominik", "Idczak"))
            array.add(DbPerson(0, "Jarosław", "Pasternak"))
            array.add(DbPerson(0, "Maciej", "Jóźwiak"))
            array.add(DbPerson(0, "Karol", "Skrzypczyk"))
            array.add(DbPerson(0, "Michał", "Obrębski"))
            array.add(DbPerson(0, "Maksymilian", "Fikus"))
            array.add(DbPerson(0, "Stanisław", "Klecha"))
            array.add(DbPerson(0, "Olaf", "Szymborski"))
            array.add(DbPerson(0, "Antoni", "Kącki"))
            array.add(DbPerson(0, "Kajetan", "Czaja"))
            array.add(DbPerson(0, "Mateusz", "Fałek"))
            array.add(DbPerson(0, "Jakub", "Pośpiech"))
            array.add(DbPerson(0, "Jan", "Deptuła"))
            array.add(DbPerson(0, "Gerwazy", "Toporowski"))
            array.add(DbPerson(0, "Tomasz", "Pietrucha"))
            array.add(DbPerson(0, "Kamil", "Major"))
            array.add(DbPerson(0, "Wojciech", "Koszyk"))
            array.add(DbPerson(0, "Szymon", "Kryś"))
            array.add(DbPerson(0, "Krzysztof", "Ogórek"))
            array.add(DbPerson(0, "Kacper", "Osmański"))
            array.add(DbPerson(0, "Mikołaj", "Słaby"))
            array.add(DbPerson(0, "Sebastian", "Idzikowski"))
            array.add(DbPerson(0, "Eryk", "Gębicki"))
            array.add(DbPerson(0, "Bartłomiej", "Banaszak"))
            array.add(DbPerson(0, "Adrian", "Najda"))
            array.add(DbPerson(0, "Hubert", "Góra"))
            array.add(DbPerson(0, "Aleksander", "Biernacki"))
            array.add(DbPerson(0, "Patryk", "Domeracki"))
            array.add(DbPerson(0, "Rafał", "Bidziński"))
            array.add(DbPerson(0, "Jacek", "Najman"))
            array.add(DbPerson(0, "Bartosz", "Orliński"))
            array.add(DbPerson(0, "Filip", "Wdowiak"))
            array.add(DbPerson(0, "Dominik", "Motyl"))
            array.add(DbPerson(0, "Jarosław", "Burakowski"))
            array.add(DbPerson(0, "Piotr", "Walczuk"))
            array.add(DbPerson(0, "Łukasz", "Szubert"))
            array.add(DbPerson(0, "Maciej", "Langowski"))
            array.add(DbPerson(0, "Karol", "Pawlas"))
            array.add(DbPerson(0, "Michał", "Machnicki"))
            array.add(DbPerson(0, "Maksymilian", "Niezgoda"))
            array.add(DbPerson(0, "Stanisław", "Warszawski"))
            array.add(DbPerson(0, "Olaf", "Adamowicz"))
            array.add(DbPerson(0, "Antoni", "Raszka"))
            array.add(DbPerson(0, "Kajetan", "Konopa"))
            array.add(DbPerson(0, "Mateusz", "Szydło"))
            array.add(DbPerson(0, "Jakub", "Dubiel"))
            array.add(DbPerson(0, "Jan", "Kordek"))
            array.add(DbPerson(0, "Tomasz", "Faliński"))
            array.add(DbPerson(0, "Kamil", "Niewiadomski"))
            array.add(DbPerson(0, "Wojciech", "Surma"))
            array.add(DbPerson(0, "Szymon", "Małkowski"))
            array.add(DbPerson(0, "Krzysztof", "Osika"))
            array.add(DbPerson(0, "Kacper", "Solak"))
            array.add(DbPerson(0, "Mikołaj", "Makówka"))
            array.add(DbPerson(0, "Sebastian", "Stolarski"))
            array.add(DbPerson(0, "Eryk", "Kulpa"))
            array.add(DbPerson(0, "Bartłomiej", "Główka"))
            array.add(DbPerson(0, "Adrian", "Cegiełka"))
            array.add(DbPerson(0, "Hubert", "Wąsowicz"))
            array.add(DbPerson(0, "Aleksander", "Brzostek"))
            array.add(DbPerson(0, "Patryk", "Tutak"))
            array.add(DbPerson(0, "Rafał", "Chrostowski"))
            array.add(DbPerson(0, "Jacek", "Kusyk"))
            array.add(DbPerson(0, "Bartosz", "Wieliczko"))
            array.add(DbPerson(0, "Filip", "Sobota"))
            array.add(DbPerson(0, "Dominik", "Wiejak"))
            array.add(DbPerson(0, "Jarosław", "Binkiewicz"))
            array.add(DbPerson(0, "Piotr", "Tobolski"))
            array.add(DbPerson(0, "Łukasz", "Kubiak"))
            array.add(DbPerson(0, "Marek", "Garczyński"))
            array.add(DbPerson(0, "Paweł", "Sitko"))
            array.add(DbPerson(0, "Maciej", "Wiktor"))
            array.add(DbPerson(0, "Karol", "Flisikowski"))
            array.add(DbPerson(0, "Michał", "Sutkowski"))
            array.add(DbPerson(0, "Maksymilian", "Bolek"))
            array.add(DbPerson(0, "Stanisław", "Lasoń"))
            array.add(DbPerson(0, "Olaf", "Kocjan"))
            array.add(DbPerson(0, "Antoni", "Serafin"))
            array.add(DbPerson(0, "Kajetan", "Głowiński"))
            array.add(DbPerson(0, "Mateusz", "Skorupa"))
            array.add(DbPerson(0, "Jakub", "Łobodziński"))
            array.add(DbPerson(0, "Jan", "Trzebiatowski"))
            array.add(DbPerson(0, "Tomasz", "Podlaski"))
            array.add(DbPerson(0, "Kamil", "Skowronek"))
            array.add(DbPerson(0, "Wojciech", "Kulczyński"))
            array.add(DbPerson(0, "Szymon", "Śliwowski"))
            array.add(DbPerson(0, "Krzysztof", "Kucharczyk"))
            array.add(DbPerson(0, "Kacper", "Wielogórski"))
            array.add(DbPerson(0, "Mikołaj", "Koralewski"))
            array.add(DbPerson(0, "Sebastian", "Kurowski"))
            array.add(DbPerson(0, "Eryk", "Dyl"))
            array.add(DbPerson(0, "Bartłomiej", "Wiliński"))
            array.add(DbPerson(0, "Adrian", "Bednarczuk"))
            array.add(DbPerson(0, "Hibert", "Wasik"))
            array.add(DbPerson(0, "Aleksander", "Kargul"))
            array.add(DbPerson(0, "Patryk", "Wypych"))
            array.add(DbPerson(0, "Rafał", "Kuźniewski"))
            array.add(DbPerson(0, "Jacek", "Zwierzchowski"))
            array.add(DbPerson(0, "Bartosz", "Śpiewak"))
            array.add(DbPerson(0, "Filip", "Książek"))
            array.add(DbPerson(0, "Dominik", "Bartosiński"))
            array.add(DbPerson(0, "Jarosław", "Kędzia"))
            array.add(DbPerson(0, "Lech", "Puzio"))
            array.add(DbPerson(0, "Piotr", "Więckowski"))
            array.add(DbPerson(0, "Łukasz", "Lubański"))
            array.add(DbPerson(0, "Marek", "Daniłowicz"))
            array.add(DbPerson(0, "Paweł", "Lewiński"))
            array.add(DbPerson(0, "Mateusz", "Pikul"))
            array.add(DbPerson(0, "Jakub", "Szyszko"))
            array.add(DbPerson(0, "Maciej", "Ryłko"))
            array.add(DbPerson(0, "Karol", "Przerwa"))
            array.add(DbPerson(0, "Michał", "Kaniecki"))
            array.add(DbPerson(0, "Antoni", "Janus"))
            array.add(DbPerson(0, "Jan", "Duszyński"))
            array.add(DbPerson(0, "Tomasz", "Rumiński"))
            array.add(DbPerson(0, "Kamil", "Tyburski"))
            array.add(DbPerson(0, "Wojciech", "Szal"))
            array.add(DbPerson(0, "Szymon", "Dominiczak"))
            array.add(DbPerson(0, "Krzysztof", "Kryński"))
            array.add(DbPerson(0, "Robert", "Kuźmicz"))
            array.add(DbPerson(0, "Kacper", "Rybarczyk"))
            array.add(DbPerson(0, "Mikołaj", "Jarosiński"))
            array.add(DbPerson(0, "Sebastian", "Strojek"))
            array.add(DbPerson(0, "Jacek", "Lubecki"))
            array.add(DbPerson(0, "Bartłomiej", "Żarski"))
            array.add(DbPerson(0, "Adrian", "Łubkowski"))
            array.add(DbPerson(0, "Hubert", "Czyżak"))
            array.add(DbPerson(0, "Aleksander", "Ziarko"))
            array.add(DbPerson(0, "Patryk", "Sitkowski"))
            array.add(DbPerson(0, "Rafał", "Małysa"))
            array.add(DbPerson(0, "Bartosz", "Golański"))
            array.add(DbPerson(0, "Filip", "Lipa"))
            array.add(DbPerson(0, "Dominik", "Grabka"))
            array.add(DbPerson(0, "Jarosław", "Szarzyński"))
            array.add(DbPerson(0, "Piotr", "Zaworski"))
            array.add(DbPerson(0, "Łukasz", "Śmiechowski"))
            array.add(DbPerson(0, "Marek", "Siekiera"))
            array.add(DbPerson(0, "Paweł", "Drewnowski"))
            array.add(DbPerson(0, "Radosłąw", "Bogusz"))
            array.add(DbPerson(0, "Marcin", "Gorzelak"))
            array.add(DbPerson(0, "Norbert", "Tomczuk"))
            array.add(DbPerson(0, "Henryk", "Szmyd"))

            return array
        }
    }

}