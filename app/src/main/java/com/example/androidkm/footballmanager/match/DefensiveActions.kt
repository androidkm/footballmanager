package com.example.androidkm.footballmanager.match

/**
 * Created by Karol on 2018-05-21.
 */
enum class DefensiveActions {
    TACKLE,
    BLOCK,
    COUNTERATTACK,
    FOUL,
    FAILURE,
}