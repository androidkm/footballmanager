package com.example.androidkm.footballmanager.utils

enum class MatchResult {
    Win,
    Draw,
    Lose
}