package com.example.androidkm.footballmanager.team

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.*
import android.widget.*
import com.example.androidkm.footballmanager.GlobalData
import com.example.androidkm.footballmanager.R
import com.example.androidkm.footballmanager.obj.PlayerBase
import com.example.androidkm.footballmanager.obj.TacticsPosition
import com.example.androidkm.footballmanager.utils.*
import kotlinx.android.synthetic.main.activity_team_tactics.*
import org.jetbrains.anko.doAsync


class TeamTacticsActivity : AppCompatActivity() {

    /**
     * The [android.support.v4.view.PagerAdapter] that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * [android.support.v4.app.FragmentStatePagerAdapter].
     */
    private var mSectionsPagerAdapter: SectionsPagerAdapter? = null
    private var isMatch: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_team_tactics)

        //setSupportActionBar(toolbar)
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = SectionsPagerAdapter(supportFragmentManager)

        if (intent.extras != null) {
            val intentValue = intent.extras.getString("MATCH")
            if (intentValue != null) {
                isMatch = true
            }
        }
        // Set up the ViewPager with the sections adapter.
        container.adapter = mSectionsPagerAdapter

        container.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabs))
        tabs.addOnTabSelectedListener(TabLayout.ViewPagerOnTabSelectedListener(container))
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_team_tactics, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId

        if (id == R.id.action_settings) {
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    /**
     * A [FragmentPagerAdapter] that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    inner class SectionsPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {


        override fun getItem(position: Int): Fragment {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position + 1, isMatch)
        }


        override fun getCount(): Int {
            return 3
        }
    }

    inner class PageChangeListener(tabLayout: TabLayout) : TabLayout.TabLayoutOnPageChangeListener(tabLayout) {

        override fun onPageSelected(position: Int) {
            super.onPageSelected(position)
        };
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    class PlaceholderFragment : Fragment() {

        var section: TeamTacticsSection = TeamTacticsSection.SQUAD
        lateinit var formationElements: List<View>
        var currentPositionIndex: Int = 0
        lateinit var list: ListView
        var isMatch: Boolean = false

        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                                  savedInstanceState: Bundle?): View? {
            var rootView: View? = null
            when (section) {
                TeamTacticsSection.SQUAD -> {
                    rootView = inflater.inflate(R.layout.fragment_team_tactics, container, false)
                    createPlayersList(rootView)
                }
                TeamTacticsSection.FORMATION -> {
                    rootView = inflater.inflate(R.layout.fragment_team_formations, container, false)
                    formationElements = createFormationElements(rootView)
                    createFormation(rootView)
                }
                TeamTacticsSection.PLAYSTYLE -> {
                    rootView = inflater.inflate(R.layout.fragment_team_playstyle, container, false)
                    initializePlaystyleValues(rootView, activity)
                    initializePlaystyleListeners(rootView, activity)
                }
            }

            return rootView
        }

        private fun createFormationElements(view: View): List<View> {
            val parent: LinearLayout = view.findViewById(R.id.formation_view)
            val viewList = ArrayList<View>()
            for (i in 0..4) {
                var row: LinearLayout = LinearLayout(context);
                row.layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f)
                parent.addView(row, 0)
                for (j in 0..4) {
                    var element: Button = Button(context)
                    element.layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 1f)
                    row.addView(element)
                    viewList.add(element)
                }
            }
            return viewList.toList();
        }

        private fun initializePlaystyleValues(view: View, activity: FragmentActivity?) {
            var seek1 = view.findViewById<SeekBar>(R.id.playstyleSeekbar)
            var seek2 = view.findViewById<SeekBar>(R.id.attackDirectionSeekbar)
            var seek3 = view.findViewById<SeekBar>(R.id.passesSeekbar)

            doAsync {
                if (activity != null) {
                    var data = (activity.application as GlobalData).getPlaystyle()
                    seek1.progress = data.playstyle
                    seek2.progress = data.attackDirection
                    seek3.progress = data.passes
                }
            }
        }

        private fun initializePlaystyleListeners(view: View, activity: FragmentActivity?) {
            var seek1 = view.findViewById<SeekBar>(R.id.playstyleSeekbar)
            var seek2 = view.findViewById<SeekBar>(R.id.attackDirectionSeekbar)
            var seek3 = view.findViewById<SeekBar>(R.id.passesSeekbar)

            seek1.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
                override fun onStopTrackingTouch(seekBar: SeekBar?) {
                }

                override fun onStartTrackingTouch(seekBar: SeekBar?) {
                }

                override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                    doAsync {
                        if (activity != null) {
                            var data = (activity.application as GlobalData).getPlaystyle()
                            data.playstyle = progress
                            (activity.application as GlobalData).setPlaystyle(data)
                        }
                    }
                }
            })

            seek2.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
                override fun onStopTrackingTouch(seekBar: SeekBar?) {
                }

                override fun onStartTrackingTouch(seekBar: SeekBar?) {
                }

                override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                    doAsync {
                        if (activity != null) {
                            var data = (activity.application as GlobalData).getPlaystyle()
                            data.attackDirection = progress
                            (activity.application as GlobalData).setPlaystyle(data)
                        }
                    }
                }
            })

            seek3.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
                override fun onStopTrackingTouch(seekBar: SeekBar?) {
                }

                override fun onStartTrackingTouch(seekBar: SeekBar?) {
                }

                override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                    doAsync {
                        if (activity != null) {
                            var data = (activity.application as GlobalData).getPlaystyle()
                            data.passes = progress
                            (activity.application as GlobalData).setPlaystyle(data)
                        }
                    }
                }
            })
        }

        companion object {
            /**
             * The fragment argument representing the section number for this
             * fragment.
             */
            private val ARG_SECTION_NUMBER = "section_number"
            private val FORMATION = "formation"
            var currentFormation: Formation = Formation.formation4231()

            /**
             * Returns a new instance of this fragment for the given section
             * number.
             */
            fun newInstance(sectionNumber: Int, isMatch: Boolean): PlaceholderFragment {
                val fragment = PlaceholderFragment()
                val args = Bundle()
                args.putInt(ARG_SECTION_NUMBER, sectionNumber)
                fragment.isMatch = isMatch
                fragment.arguments = args
                when (sectionNumber) {
                    1 -> fragment.section = TeamTacticsSection.SQUAD
                    2 -> fragment.section = TeamTacticsSection.FORMATION
                    3 -> fragment.section = TeamTacticsSection.PLAYSTYLE
                }
                return fragment
            }
        }

        fun createPlayersList(view: View) {
            list = view.findViewById(R.id.tactics_list)
            val header = layoutInflater.inflate(R.layout.player_tactics_view, null)
            list.addHeaderView(header)

            //val positionAdapter = PositionAdapter(view.context, reorganizeList(currentFormation));
            if (!currentFormation.hasMaxPlayers()) {
                currentFormation.autoComplete()
                (activity!!.application as GlobalData).setMatchSquad(currentFormation)
            }
            (activity!!.application as GlobalData).clearMatchSquad()
            val matchSquad = (activity!!.application as GlobalData).getMatchSquad()
            val positionAdapter = PositionAdapter(view.context, matchSquad)

            list.adapter = positionAdapter

            list.setOnItemClickListener { parent, view, position, id ->
                val intent = Intent(context, PlayerSelectActivity::class.java)
                val b = Bundle()
                val currentTacticalPosition = list.adapter.getItem(position) as TacticsPosition
                if (!(currentTacticalPosition.player == null && isMatch)) {
                    currentPositionIndex = position
                    b.putSerializable("POS", currentTacticalPosition.fullPosition)
                    b.putSerializable("PLAYER", currentTacticalPosition.player)
                    intent.putExtras(b)
                    startActivityForResult(intent, 1)
                }
            }
        }

        override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
            super.onActivityResult(requestCode, resultCode, data)
            if (resultCode == RESULT_OK && data != null) {
                val player = data.getSerializableExtra("PLAYER") as PlayerBase
                (list.adapter.getItem(currentPositionIndex) as TacticsPosition).player = player
                ((list.adapter as HeaderViewListAdapter).wrappedAdapter as PositionAdapter).refresh()
                val squad = (activity!!.application as GlobalData).getMatchSquad()
                squad[currentPositionIndex - 1].player = player
            }
        }

        fun createFormation(view: View) {

            view.findViewById<View>(R.id.formation_defenders).findViewById<TextView>(R.id.position_name).text = "DEF"
            view.findViewById<View>(R.id.formation_midfielders_def).findViewById<TextView>(R.id.position_name).text = "DM"
            view.findViewById<View>(R.id.formation_midfielders).findViewById<TextView>(R.id.position_name).text = "MID"
            view.findViewById<View>(R.id.formation_midfielders_atk).findViewById<TextView>(R.id.position_name).text = "AM"
            view.findViewById<View>(R.id.formation_forwards).findViewById<TextView>(R.id.position_name).text = "F"

            view.findViewById<View>(R.id.formation_defenders).findViewById<TextView>(R.id.formation_players_count).text =
                    currentFormation.backPlayers.toString()
            view.findViewById<View>(R.id.formation_midfielders_def).findViewById<TextView>(R.id.formation_players_count).text =
                    currentFormation.defesivePlayers.toString()
            view.findViewById<View>(R.id.formation_midfielders).findViewById<TextView>(R.id.formation_players_count).text =
                    currentFormation.midfieldPlayers.toString()
            view.findViewById<View>(R.id.formation_midfielders_atk).findViewById<TextView>(R.id.formation_players_count).text =
                    currentFormation.offensivePlayers.toString()
            view.findViewById<View>(R.id.formation_forwards).findViewById<TextView>(R.id.formation_players_count).text =
                    currentFormation.forwardPlayers.toString()

            view.findViewById<View>(R.id.formation_defenders).findViewById<Button>(R.id.button_plus)
                    .setOnClickListener(FormationAddButtonListener(view, view.findViewById<View>(R.id.formation_defenders), true))
            view.findViewById<View>(R.id.formation_defenders).findViewById<Button>(R.id.button_minus)
                    .setOnClickListener(FormationAddButtonListener(view, view.findViewById<View>(R.id.formation_defenders), false))
            view.findViewById<View>(R.id.formation_midfielders_def).findViewById<Button>(R.id.button_plus)
                    .setOnClickListener(FormationAddButtonListener(view, view.findViewById<View>(R.id.formation_midfielders_def), true))
            view.findViewById<View>(R.id.formation_midfielders_def).findViewById<Button>(R.id.button_minus)
                    .setOnClickListener(FormationAddButtonListener(view, view.findViewById<View>(R.id.formation_midfielders_def), false))
            view.findViewById<View>(R.id.formation_midfielders).findViewById<Button>(R.id.button_plus)
                    .setOnClickListener(FormationAddButtonListener(view, view.findViewById<View>(R.id.formation_midfielders), true))
            view.findViewById<View>(R.id.formation_midfielders).findViewById<Button>(R.id.button_minus)
                    .setOnClickListener(FormationAddButtonListener(view, view.findViewById<View>(R.id.formation_midfielders), false))
            view.findViewById<View>(R.id.formation_midfielders_atk).findViewById<Button>(R.id.button_plus)
                    .setOnClickListener(FormationAddButtonListener(view, view.findViewById<View>(R.id.formation_midfielders_atk), true))
            view.findViewById<View>(R.id.formation_midfielders_atk).findViewById<Button>(R.id.button_minus)
                    .setOnClickListener(FormationAddButtonListener(view, view.findViewById<View>(R.id.formation_midfielders_atk), false))
            view.findViewById<View>(R.id.formation_forwards).findViewById<Button>(R.id.button_plus)
                    .setOnClickListener(FormationAddButtonListener(view, view.findViewById<View>(R.id.formation_forwards), true))
            view.findViewById<View>(R.id.formation_forwards).findViewById<Button>(R.id.button_minus)
                    .setOnClickListener(FormationAddButtonListener(view, view.findViewById<View>(R.id.formation_forwards), false))

            for (element in formationElements) {
                element.visibility = View.INVISIBLE
            }

            for (position in currentFormation.positions) {
                if (position.pos == Position.GK) continue
                val pos: Int = PositionTranlator.toInt(position.pos)
                val side: Int = PositionTranlator.toInt(position.side)
                formationElements[getFormationElementIndex(pos, side)].visibility = View.VISIBLE
                formationElements[getFormationElementIndex(pos, side)].background = resources.getDrawable(R.drawable.formation_element)
            }

        }

        fun getFormationElementIndex(pos: Int, side: Int): Int {
            return pos * 5 + side
        }

        inner class FormationAddButtonListener(val mainView: View, val currentView: View, val isPlus: Boolean) : View.OnClickListener {
            override fun onClick(v: View?) {
                if (isMatch) return
                var counter = (currentView.findViewById<TextView>(R.id.formation_players_count).text as String).toInt()
                if (isPlus) {
                    if (currentFormation.hasMaxPlayers() || counter == 5) {
                        return
                    }
                    counter++
                } else {
                    if (counter == 0) return
                    counter--
                }
                currentView.findViewById<TextView>(R.id.formation_players_count).text = counter.toString()

                val bckCounter = (mainView.findViewById<View>(R.id.formation_defenders).findViewById<TextView>(R.id.formation_players_count).text as String).toInt()
                val defCounter = (mainView.findViewById<View>(R.id.formation_midfielders_def).findViewById<TextView>(R.id.formation_players_count).text as String).toInt()
                val midCounter = (mainView.findViewById<View>(R.id.formation_midfielders).findViewById<TextView>(R.id.formation_players_count).text as String).toInt()
                val offCounter = (mainView.findViewById<View>(R.id.formation_midfielders_atk).findViewById<TextView>(R.id.formation_players_count).text as String).toInt()
                val forCounter = (mainView.findViewById<View>(R.id.formation_forwards).findViewById<TextView>(R.id.formation_players_count).text as String).toInt()

                currentFormation = Formation()
                currentFormation.backPlayers = bckCounter
                currentFormation.defesivePlayers = defCounter
                currentFormation.midfieldPlayers = midCounter
                currentFormation.offensivePlayers = offCounter
                currentFormation.forwardPlayers = forCounter
                currentFormation.createFormationByPositionsCounter()

                for (element in formationElements) {
                    element.visibility = View.INVISIBLE
                }

                for (position in currentFormation.positions) {
                    if (position.pos == Position.GK) continue;
                    val pos: Int = PositionTranlator.toInt(position.pos)
                    val side: Int = PositionTranlator.toInt(position.side)
                    formationElements[getFormationElementIndex(pos, side)].visibility = View.VISIBLE
                    formationElements[getFormationElementIndex(pos, side)].background = resources.getDrawable(R.drawable.formation_element)
                }
                if (activity != null) {
                    (activity!!.application as GlobalData).setMatchSquad(currentFormation)
                }
            }
        }


        inner class FormationAdapter : BaseAdapter {

            private var list = ArrayList<Formation>()
            private var context: Context? = null

            constructor(context: Context, list: ArrayList<Formation>) : super() {
                this.list = list
                this.context = context
            }

            override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {

                val view: View?
                val vh: ViewHolderFormation

                if (convertView == null) {
                    view = layoutInflater.inflate(R.layout.formation, parent, false)
                    vh = ViewHolderFormation(view)
                    view.tag = vh
                    Log.i("JSA", "set Tag for ViewHolder, position: " + position)
                } else {
                    view = convertView
                    vh = view.tag as ViewHolderFormation
                }

                vh.formation.text = list[position].name;

                return view
            }

            override fun getItem(position: Int): Formation {
                return list[position]
            }

            override fun getItemId(position: Int): Long {
                return position.toLong()
            }

            override fun getCount(): Int {
                return list.size
            }
        }

        private class ViewHolderFormation(view: View?) {
            val formation: TextView

            init {
                formation = view?.findViewById<TextView>(R.id.formation) as TextView
            }
        }

        inner class PositionAdapter : BaseAdapter {

            private var list = ArrayList<TacticsPosition>()
            private var context: Context? = null

            constructor(context: Context, notesList: ArrayList<TacticsPosition>) : super() {
                this.list = notesList
                this.context = context
            }

            override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {

                val view: View?
                val vh: ViewHolder

                if (convertView == null) {
                    view = layoutInflater.inflate(R.layout.player_tactics_view, parent, false)
                    vh = ViewHolder(view)
                    view.tag = vh
                    Log.i("JSA", "set Tag for ViewHolder, position: " + position)
                } else {
                    view = convertView
                    vh = view.tag as ViewHolder
                }

                val player = list[position].player
                vh.formationPosition.text = list[position].getPositon()
                if (player != null) {
                    vh.playerPosition.text = player.getPosition()
                    vh.playerFullName.text = player.getFullName()
                    vh.playerOverall.text = player.getOverall().toString()
                    vh.playerCondition.text = player.condition.toString()
                    if (player.position == list[position].fullPosition.pos && player.side == list[position].fullPosition.side) {
                        vh.playerPosition.setTextColor(resources.getColor(R.color.goodPosition))
                    } else if (player.position == list[position].fullPosition.pos || player.side == list[position].fullPosition.side) {
                        vh.playerPosition.setTextColor(resources.getColor(R.color.averagePosition))
                    } else {
                        vh.playerPosition.setTextColor(resources.getColor(R.color.badPosition))
                    }
                } else {
                    vh.playerFullName.text = "EMPTY"
                    vh.playerPosition.text = " "
                    vh.playerOverall.text = " "
                    vh.playerCondition.text = " "
                }

                return view
            }

            override fun getItem(position: Int): Any {
                return list[position]
            }

            override fun getItemId(position: Int): Long {
                return position.toLong()
            }

            override fun getCount(): Int {
                return list.size
            }

            fun refresh() {
                notifyDataSetChanged()
            }
        }

        private class ViewHolder(view: View?) {
            val formationPosition: TextView
            val playerPosition: TextView
            val playerFullName: TextView
            val playerOverall: TextView
            val playerCondition: TextView

            init {
                formationPosition = view?.findViewById<TextView>(R.id.formationPosition) as TextView
                playerPosition = view?.findViewById<TextView>(R.id.playerPositionView) as TextView
                playerFullName = view?.findViewById<TextView>(R.id.playerNameView) as TextView
                playerOverall = view?.findViewById<TextView>(R.id.playerOverallView) as TextView
                playerCondition = view?.findViewById<TextView>(R.id.playerConditionView) as TextView
            }
        }
    }
}
