package com.example.androidkm.footballmanager.utils

import com.example.androidkm.footballmanager.utils.Position
import com.example.androidkm.footballmanager.utils.Side
import java.io.Serializable

/**
 * Created by Karol on 2018-04-24.
 */
class Formation : Serializable {

    var positions: ArrayList<FullPosition> = ArrayList(11)
    var name: String = ""
    var forwardPlayers: Int = 0
    var offensivePlayers: Int = 0
    var midfieldPlayers: Int = 0
    var defesivePlayers: Int = 0
    var backPlayers: Int = 0

    companion object {
        fun formation442(): Formation {
            var formation = Formation()
            formation.add1Player(Position.GK)
            formation.add4Players(Position.B)
            formation.add4Players(Position.M)
            formation.add2Players(Position.F)
            formation.name = "4-4-2"
            formation.forwardPlayers = 2
            formation.midfieldPlayers = 4
            formation.backPlayers = 4
            return formation
        }

        fun formation433(): Formation {
            var formation = Formation()
            formation.add1Player(Position.GK)
            formation.add4Players(Position.B)
            formation.add3Players(Position.M)
            formation.add3PlayersWide(Position.F)
            formation.name = "4-3-3"
            formation.forwardPlayers = 3
            formation.midfieldPlayers = 3
            formation.backPlayers = 4
            return formation
        }

        fun formation4231(): Formation {
            var formation = Formation()
            formation.add1Player(Position.GK)
            formation.add4Players(Position.B)
            formation.add2Players(Position.DM)
            formation.add3PlayersWide(Position.AM)
            formation.add1Player(Position.F)
            formation.name = "4-2-3-1"
            formation.forwardPlayers = 1
            formation.defesivePlayers = 2
            formation.offensivePlayers = 3
            formation.backPlayers = 4
            return formation
        }
    }

    fun createFormationByPositionsCounter() {
        add1Player(Position.GK)
        when (backPlayers) {
            1 -> add1Player(Position.B)
            2 -> add2Players(Position.B)
            3 -> add3PlayersWide(Position.B)
            4 -> add4Players(Position.B)
            5 -> add5Players(Position.B)
        }
        when (defesivePlayers) {
            1 -> add1Player(Position.DM)
            2 -> add2Players(Position.DM)
            3 -> add3PlayersWide(Position.DM)
            4 -> add4Players(Position.DM)
            5 -> add5Players(Position.DM)
        }
        when (midfieldPlayers) {
            1 -> add1Player(Position.M)
            2 -> add2Players(Position.M)
            3 -> add3PlayersWide(Position.M)
            4 -> add4Players(Position.M)
            5 -> add5Players(Position.M)
        }
        when (offensivePlayers) {
            1 -> add1Player(Position.AM)
            2 -> add2Players(Position.AM)
            3 -> add3PlayersWide(Position.AM)
            4 -> add4Players(Position.AM)
            5 -> add5Players(Position.AM)
        }
        when (forwardPlayers) {
            1 -> add1Player(Position.F)
            2 -> add2Players(Position.F)
            3 -> add3PlayersWide(Position.F)
            4 -> add4Players(Position.F)
            5 -> add5Players(Position.F)
        }
    }

    fun hasMaxPlayers(): Boolean {
        return (backPlayers + midfieldPlayers + defesivePlayers + offensivePlayers + forwardPlayers) >= 10
    }

    fun add5Players(pos: Position) {
        positions.add(FullPosition(pos, Side.L))
        positions.add(FullPosition(pos, Side.LC))
        positions.add(FullPosition(pos, Side.C))
        positions.add(FullPosition(pos, Side.RC))
        positions.add(FullPosition(pos, Side.R))
    }

    fun add4Players(pos: Position) {
        positions.add(FullPosition(pos, Side.L))
        positions.add(FullPosition(pos, Side.LC))
        positions.add(FullPosition(pos, Side.RC))
        positions.add(FullPosition(pos, Side.R))
    }

    fun add3Players(pos: Position) {
        positions.add(FullPosition(pos, Side.LC))
        positions.add(FullPosition(pos, Side.C))
        positions.add(FullPosition(pos, Side.RC))
    }

    fun add3PlayersWide(pos: Position) {
        positions.add(FullPosition(pos, Side.L))
        positions.add(FullPosition(pos, Side.C))
        positions.add(FullPosition(pos, Side.R))
    }

    fun add2Players(pos: Position) {
        positions.add(FullPosition(pos, Side.LC))
        positions.add(FullPosition(pos, Side.RC))
    }

    fun add2PlayersWide(pos: Position) {
        positions.add(FullPosition(pos, Side.L))
        positions.add(FullPosition(pos, Side.R))
    }

    fun add1Player(pos: Position) {
        positions.add(FullPosition(pos, Side.C))
    }

    fun autoComplete() {
        var playersNeeded = 10 - (backPlayers + midfieldPlayers + defesivePlayers + offensivePlayers + forwardPlayers)

        while (playersNeeded > 0) {
            if (backPlayers < 4) {
                backPlayers++
                playersNeeded--
            } else if (midfieldPlayers < 4) {
                midfieldPlayers++
                playersNeeded--
            } else {
                forwardPlayers++
                playersNeeded--
            }
        }
        positions = ArrayList(11)
        createFormationByPositionsCounter()
    }
}

class FullPosition(var pos: Position, var side: Side?) : Serializable {
    override fun toString(): String {
        if (pos != Position.GK)
            return side.toString() + pos.toString()
        return pos.toString()
    }
}