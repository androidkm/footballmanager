package com.example.androidkm.footballmanager.utils

enum class UpgradeEnum{
    GoalkeepersTrainer,
    DefendersTrainer,
    MidfieldersTrainer,
    StrikersTrainer,
    TeamDoctor,
    Headhunter,
    StadiumSeats,
    StadiumAttractions,
    RecoveryFacility,
    YouthAcademy
}