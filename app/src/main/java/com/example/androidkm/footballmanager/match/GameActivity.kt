package com.example.androidkm.footballmanager.match

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.TextView
import android.widget.Toast
import com.example.androidkm.footballmanager.GlobalData
import com.example.androidkm.footballmanager.R
import com.example.androidkm.footballmanager.database.DataBaseHelper
import com.example.androidkm.footballmanager.main_menu.MainMenuActivity
import com.example.androidkm.footballmanager.obj.PlayerBase
import com.example.androidkm.footballmanager.obj.TacticsPosition
import com.example.androidkm.footballmanager.team.TeamTacticsActivity
import com.example.androidkm.footballmanager.utils.*
import org.jetbrains.anko.contentView
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.util.*

class GameActivity : AppCompatActivity() {

    lateinit var engine: MatchEngine

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game)

        val myListView = findViewById<ListView>(R.id.matchLogList)
        val adapter = MatchLogAdapter(this, R.layout.match_log_entry_view, createSimpleData())
        (this.application as GlobalData).completeMatchSquad()
        val playersTeam = ArrayList<TacticsPosition>()
        playersTeam.addAll((this.application as GlobalData).getMatchSquad())
        val opponentsTeam = ArrayList<TacticsPosition>()
        val opponentFormation = Formation.formation442()
        val context = this
        doAsync {
            val players = DataBaseHelper.getAllFreePlayers(applicationContext)
            opponentsTeam.add(TacticsPosition(FullPosition(Position.GK, Side.C), players[0]))
            var i = 1
            var matchNumber = ((context.application as GlobalData).getMatchNumber() + ((context.application as GlobalData).getSeasonNumber() * 30))
            for (pos in opponentFormation.positions) {
                players[i].skills.change(matchNumber, 1.0)
                opponentsTeam.add(TacticsPosition(pos, players[i]))
                i++
            }
            uiThread {
                engine = MatchEngine(adapter, context, applicationContext, contentView!!, arrayOf(playersTeam, opponentsTeam))
                engine.run()
                myListView.adapter = adapter
            }
        }
    }

    private fun createSimpleData(): ArrayList<MatchLogEntry> {
        val array = ArrayList<MatchLogEntry>()
        return array
    }

    inner class MatchLogAdapter(context: Context?, resource: Int, objects: ArrayList<MatchLogEntry>) : ArrayAdapter<MatchLogEntry>(context, resource, objects) {

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {

            var v = convertView
            if (v == null) {
                val vi: LayoutInflater = LayoutInflater.from(context)
                v = vi.inflate(R.layout.match_log_entry_view, null)
            }

            val p = getItem(position)
            if (p != null) {

                val tvMinute = v!!.findViewById<TextView>(R.id.matchLogMinute)
                val tvText = v.findViewById<TextView>(R.id.matchLogText)

                tvMinute.text = String.format("%d'", p.minute)
                tvText.text = p.text
            }

            return v
        }
    }

    fun tacticsClick(view: View) {
        val intent = Intent(this@GameActivity, TeamTacticsActivity::class.java)
        intent.putExtra("MATCH", "YES")
        startActivity(intent)
    }

    fun skipMatchClick(view: View) {
        startActivity(Intent(this@GameActivity, MainMenuActivity::class.java))

        val r = Random()
        val minute = engine.currentMinute
        if (minute < 89) {
            val randomMax = ((90 - minute) / 30) + 2
            engine.results[0] += r.nextInt(randomMax)
            engine.results[1] += r.nextInt(randomMax)
        }
        val gb = (this.application as GlobalData)
        var isLost = saveMatchResults()
        actualizePlayersStatus()
        gb.increaseCurrentRound()
        addCashAfterMatch(isLost)
        doAsync { gb.saveAllData() }
        Toast.makeText(this, "Match ended with result ${engine.results[0]}:${engine.results[1]}", Toast.LENGTH_LONG).show()
    }

    private fun saveMatchResults(): Boolean {

        val oppositeTeamId = simulateOtherResults()
        val globalData = (this.application as GlobalData)

        val leftTeamScore = engine.results[0]
        val rightTeamScore = engine.results[1]

        var result: String
        var lose: Boolean = true

        when {
            leftTeamScore > rightTeamScore -> {
                result = "W"
                globalData.setMatchResult(globalData.getTeamId(), MatchResult.Win)
                globalData.setMatchResult(oppositeTeamId, MatchResult.Lose)
                lose = false
            }
            leftTeamScore < rightTeamScore -> {
                result = "L"
                globalData.setMatchResult(globalData.getTeamId(), MatchResult.Lose)
                globalData.setMatchResult(oppositeTeamId, MatchResult.Win)
            }
            else -> {
                result = "D"
                globalData.setMatchResult(globalData.getTeamId(), MatchResult.Draw)
                globalData.setMatchResult(oppositeTeamId, MatchResult.Draw)
            }
        }

        (this.application as GlobalData).newTrainings()
        (this.application as GlobalData).addLastMatch(result)

        return lose
    }

    private fun simulateOtherResults(): Int {

        val globalData = (this.application as GlobalData)
        val matches = globalData.getCurrentRound()
        val mainTeamId = globalData.getTeamId()

        var oppositeTeamId = 0

        for (match in matches) {

            if (match.first == mainTeamId) {
                oppositeTeamId = match.second
                continue
            }

            if (match.second == mainTeamId) {
                oppositeTeamId = match.first
                continue
            }

            val rand = Random().nextInt(9) + 1

            when {
                rand < 5 -> {
                    globalData.setMatchResult(match.first, MatchResult.Win)
                    globalData.setMatchResult(match.second, MatchResult.Lose)
                }
                rand > 7 -> {
                    globalData.setMatchResult(match.second, MatchResult.Win)
                    globalData.setMatchResult(match.first, MatchResult.Lose)
                }
                else -> {
                    globalData.setMatchResult(match.first, MatchResult.Draw)
                    globalData.setMatchResult(match.second, MatchResult.Draw)
                }
            }
        }
        return oppositeTeamId
    }

    private fun addCashAfterMatch(isLost: Boolean) {
        val globalData = (this.application as GlobalData)

        val stadiumSeatsLevel = globalData.getUpgrade(UpgradeEnum.StadiumSeats)
        val stadiumAttractionsLevel = globalData.getUpgrade(UpgradeEnum.StadiumAttractions)

        val money: Long = (calculateIncome(stadiumSeatsLevel, isLost)) + Random().nextInt(calculateIncome(stadiumAttractionsLevel, isLost)).toLong()
        globalData.addMoney(money)
    }

    private fun calculateIncome(currentLevel: Int, isLost: Boolean): Int {
        var levelZero: Int = if (isLost) {
            1
        } else {
            10
        }

        for (i in 0..currentLevel) {
            levelZero *= 10
        }
        return levelZero
    }

    private fun actualizePlayersStatus() {
        val squad = (this.application as GlobalData).getSquad()
        for (i in 0..(squad.count() - 1)) {
            if (squad[i].status == PauseStatus.YELLOW_CARD && squad[i].yellowCards < 2) {
                squad[i].status = PauseStatus.READY_TO_PLAY
                squad[i].yellowCards++
            } else if (squad[i].status == PauseStatus.YELLOW_CARD) {
                squad[i].pauseMatch = 1
                squad[i].yellowCards = 0
            } else if (squad[i].pauseMatch > 0) {
                squad[i].pauseMatch--
            }
            if (squad[i].pauseMatch <= 0) {
                squad[i].status = PauseStatus.READY_TO_PLAY
                squad[i].pauseMatch = 0
            }
        }
    }

    override fun onBackPressed() {

    }
}
