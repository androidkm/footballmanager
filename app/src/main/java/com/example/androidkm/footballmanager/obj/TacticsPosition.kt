package com.example.androidkm.footballmanager.obj

import com.example.androidkm.footballmanager.utils.FullPosition
import com.example.androidkm.footballmanager.utils.Position
import com.example.androidkm.footballmanager.utils.Side

/**
 * Created by Karol on 2018-04-10.
 */
class TacticsPosition(val fullPosition: FullPosition, var player: PlayerBase?) {

    fun getPositon(): String {
        if (fullPosition.pos != Position.GK)
        return fullPosition.side.toString() + fullPosition.pos.toString()
        return fullPosition.pos.toString()
    }
}