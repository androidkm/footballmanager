package com.example.androidkm.footballmanager

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.view.View
import android.widget.Toast
import com.example.androidkm.footballmanager.database.DataBaseHelper
import com.example.androidkm.footballmanager.main_menu.MainMenuActivity
import com.example.androidkm.footballmanager.main_menu.NewGameActivity
import org.jetbrains.anko.doAsync

class WelcomeScreenActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome_screen)
    }

    fun StartNewGame(view: View){

        var alert = AlertDialog.Builder(this)
        alert.setTitle("Creating new game")
        alert.setMessage("Do you want to create new game ? This will overwrite your data.")
        alert.setPositiveButton("Create", {_,_ ->
            startActivity(Intent(this@WelcomeScreenActivity, NewGameActivity::class.java))
        })
        alert.setNegativeButton("Cancel", {_,_ ->}) // pass action

        alert.create().show()
    }

    fun LoadGame(view: View){
        try {
            (this.application as GlobalData).loadAllData()
            startActivity(Intent(this@WelcomeScreenActivity, MainMenuActivity::class.java))
        } catch (e: Exception){
            Toast.makeText(applicationContext,"Cannot find create game, please use New Game option",Toast.LENGTH_LONG).show()
        }
    }

    fun ExitApplication(view: View) {

        (this.application as GlobalData).saveAllData()

        moveTaskToBack(true)
        finish()
    }
}
