package com.example.androidkm.footballmanager.utils

/**
 * Created by Karol on 2018-06-04.
 */
enum class TrainingType {
    BALANCE, SHOOTING, PASSING, SPEED, DRIBBLING, DEFENCE, GOALKEEPING
}