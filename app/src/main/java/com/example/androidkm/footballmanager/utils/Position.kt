package com.example.androidkm.footballmanager.utils

/**
 * Created by Karol on 2018-04-08.
 */
enum class Position {
    GK, B, DM, M, AM, F
}

enum class Side {
    L, LC, C, RC, R
}

class PositionTranlator {
    companion object {

        fun toInt(pos: Position): Int {
            return when (pos) {
                Position.B -> 0
                Position.DM -> 1
                Position.M -> 2
                Position.AM -> 3
                Position.F -> 4
                Position.GK -> 0
            }
        }

        fun toInt(side: Side?): Int {
            if (side == null) return 2
            return when (side) {
                Side.L -> 0
                Side.LC -> 1
                Side.C -> 2
                Side.RC -> 3
                Side.R -> 4
            }
        }
    }
}