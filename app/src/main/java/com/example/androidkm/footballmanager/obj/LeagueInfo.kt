package com.example.androidkm.footballmanager.obj

import android.content.Context
import com.example.androidkm.footballmanager.database.DataBaseHelper
import org.jetbrains.anko.doAsync

class LeagueInfo constructor(var teams: ArrayList<TeamBase> = ArrayList(),
                             var rounds: ArrayList<ArrayList<Pair<Int, Int>>> = ArrayList(),
                             var currentRoundIndex: Int = 0,
                             var currentSeason: Int = 0) {

    constructor(context: Context): this(){
        //doAsync {
            rounds = arrayListOf(
                    arrayListOf(Pair(1,2), Pair(3,4), Pair(5,6), Pair(7,8), Pair(9,10), Pair(11,12), Pair(13,14), Pair(15,16)),
                    arrayListOf(Pair(14,9), Pair(4,11), Pair(6,3), Pair(2,5), Pair(8,15), Pair(1,13), Pair(12,7), Pair(16,10)),
                    arrayListOf(Pair(11,16), Pair(10,2), Pair(8,14), Pair(7,4), Pair(9,1), Pair(3,12), Pair(13,6), Pair(15,5)),
                    arrayListOf(Pair(6,10), Pair(1,4), Pair(2,7), Pair(16,3), Pair(11,8), Pair(12,14), Pair(5,13), Pair(9,15)),
                    arrayListOf(Pair(8,6), Pair(14,1), Pair(13,16), Pair(4,5), Pair(7,9), Pair(15,12), Pair(3,2), Pair(10,11)),
                    arrayListOf(Pair(2,14), Pair(5,7), Pair(9,3), Pair(6,15), Pair(12,10), Pair(1,8), Pair(11,13), Pair(16,4)),
                    arrayListOf(Pair(7,1), Pair(10,5), Pair(15,11), Pair(12,6), Pair(4,2), Pair(13,9), Pair(14,3), Pair(8,16)),
                    arrayListOf(Pair(16,1), Pair(9,4), Pair(15,14), Pair(10,8), Pair(5,12), Pair(11,7), Pair(3,13), Pair(6,2)),
                    arrayListOf(Pair(9,16), Pair(13,15), Pair(1,3), Pair(7,10), Pair(4,6), Pair(14,11), Pair(8,5), Pair(2,12)),
                    arrayListOf(Pair(6,9), Pair(10,13), Pair(11,2), Pair(3,7), Pair(15,4), Pair(16,14), Pair(12,8), Pair(5,1)),
                    arrayListOf(Pair(14,10), Pair(9,5), Pair(2,16), Pair(3,15), Pair(4,12), Pair(7,6), Pair(1,11), Pair(13,8)),
                    arrayListOf(Pair(15,1), Pair(12,13), Pair(6,14), Pair(11,9), Pair(10,4), Pair(16,7), Pair(8,2), Pair(5,3)),
                    arrayListOf(Pair(16,6), Pair(7,15), Pair(14,5), Pair(3,11), Pair(2,13), Pair(9,12), Pair(4,8), Pair(1,10)),
                    arrayListOf(Pair(10,3), Pair(13,4), Pair(6,1), Pair(5,11), Pair(14,7), Pair(12,16), Pair(8,9), Pair(2,15)),
                    arrayListOf(Pair(15,10), Pair(9,2), Pair(1,12), Pair(4,14), Pair(7,13), Pair(11,6), Pair(16,5), Pair(3,8))
                    )

            teams.addAll(DataBaseHelper.getAllTeams(context))
            var temp: ArrayList<ArrayList<Pair<Int,Int>>> = ArrayList()

            for(item: ArrayList<Pair<Int,Int>> in rounds){

                var temp2: ArrayList<Pair<Int,Int>> = ArrayList()

                for(item2: Pair<Int,Int> in item){

                    temp2.add(Pair(item2.second, item2.first))
                }
                temp.add(temp2)
            }

            for (item3 in temp){
                rounds.add(item3)
            }

        //}

    }
}
