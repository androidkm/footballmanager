package com.example.androidkm.footballmanager.obj

class TeamUpgrades(var gTrainerLevel:Int = 0,
                   var dTrainerLevel: Int = 0,
                   var mTrainerLevel: Int = 0,
                   var sTrainerLevel: Int = 0,
                   var doctorLevel: Int = 0,
                   var headhunterLevel: Int = 0,
                   var stadiumLevel: Int = 0,
                   var attractionsLevel: Int = 0,
                   var recoveryLevel: Int = 0,
                   var academyLevel: Int = 0)