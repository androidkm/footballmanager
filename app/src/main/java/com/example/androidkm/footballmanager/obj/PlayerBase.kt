package com.example.androidkm.footballmanager.obj

import com.example.androidkm.footballmanager.utils.PauseStatus
import com.example.androidkm.footballmanager.utils.Position
import com.example.androidkm.footballmanager.utils.Side
import com.example.androidkm.footballmanager.utils.TrainingType
import java.io.Serializable
import java.util.*

/**
 * Created by Karol on 2018-04-08.
 */
open class PlayerBase(var id: Long, var number: Int, var position: Position, var side: Side, var name: String, var surname: String,
                      var condition: Int) : Serializable {

    var skills = Skills.create(null)
    var status: PauseStatus = PauseStatus.READY_TO_PLAY
    var pauseMatch: Int = 0
    var yellowCards: Int = 0
    var currentTrainng: TrainingType = TrainingType.BALANCE

    constructor(player: PlayerBase) : this(player.id, player.number, player.position, player.side, player.name, player.surname,
            player.condition)

    fun getPosition(): String {
        if (position != Position.GK)
            return side.toString() + position.toString()
        return position.toString()
    }

    fun getFullName(): String {
        return name + " " + surname;
    }

    fun getOverall(): Double {
        if (position == Position.GK) {
            return (((skills.gkOverall() * 100).toInt())).toDouble() / 100.0
        } else {
            return (((skills.fieldOverall() * 100).toInt())).toDouble() / 100.0
        }
    }

    fun getValue(): Long {
        return (getOverall() * 10000).toLong()
    }

    fun getValueString(): String {
        val value = getValue()
        val millions = value / 1000000
        val thousands = (value - millions * 1000000) / 1000
        var thousandszeros = ""
        if (thousands < 10) {
            thousandszeros = "00"
        } else if (thousands < 100) {
            thousandszeros = "0"
        }
        var millionsString = ""
        if (millions > 0) {
            millionsString = millions.toString() + "."
        }
        return millionsString + thousandszeros + thousands.toString() + ".000 "
    }

    fun isAbleToPlay(): Boolean {
        return this.status == PauseStatus.READY_TO_PLAY
    }
}