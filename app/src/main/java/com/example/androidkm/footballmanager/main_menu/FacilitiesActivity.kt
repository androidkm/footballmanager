package com.example.androidkm.footballmanager.main_menu

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import android.widget.Toast
import com.example.androidkm.footballmanager.GlobalData
import com.example.androidkm.footballmanager.R
import com.example.androidkm.footballmanager.utils.UpgradeEnum

class FacilitiesActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_facilities)

        val globalData = (this.application as GlobalData)
        setStadiumSeatsMoney(globalData.getUpgrade(UpgradeEnum.StadiumSeats))
        setStadiumAttractionMoney(globalData.getUpgrade(UpgradeEnum.StadiumAttractions))
        setRecoveryFacilityMoney(globalData.getUpgrade(UpgradeEnum.RecoveryFacility))
        setYouthAcademyMoney(globalData.getUpgrade(UpgradeEnum.YouthAcademy))
    }

    fun upgradeStadiumSeats(view: View){
        val globalData = (this.application as GlobalData)
        val currentLevel = globalData.getUpgrade(UpgradeEnum.StadiumSeats)
        if(globalData.removeMoney(calculateUpgradeCost(currentLevel + 1))){
            globalData.setUpgrade(currentLevel + 1, UpgradeEnum.StadiumSeats)
            setStadiumSeatsMoney(currentLevel + 1)
            Toast.makeText(this, "Stadium seats upgraded to ${currentLevel+1} level", Toast.LENGTH_SHORT).show()
        }
    }

    private fun setStadiumSeatsMoney(currentLevel: Int){
        val tv = findViewById<TextView>(R.id.stadium_upgrade_one_money)
        val text = "${calculateUpgradeCost(currentLevel + 1)} $"
        tv.text = text
    }

    fun upgradeStadiumAttractions(view: View){
        val globalData = (this.application as GlobalData)
        val currentLevel = globalData.getUpgrade(UpgradeEnum.StadiumAttractions)
        if(globalData.removeMoney(calculateUpgradeCost(currentLevel + 1))){
            globalData.setUpgrade(currentLevel + 1, UpgradeEnum.StadiumAttractions)
            setStadiumAttractionMoney(currentLevel + 1)
            Toast.makeText(this, "Stadium attractions upgraded to ${currentLevel+1} level", Toast.LENGTH_SHORT).show()
        }
    }

    private fun setStadiumAttractionMoney(currentLevel: Int){
        val tv = findViewById<TextView>(R.id.stadium_upgrade_two_money)
        val text = "${calculateUpgradeCost(currentLevel + 1)} $"
        tv.text = text
    }

    fun upgradeRecoveryFacility(view: View){
        val globalData = (this.application as GlobalData)
        val currentLevel = globalData.getUpgrade(UpgradeEnum.RecoveryFacility)
        if(globalData.removeMoney(calculateUpgradeCost(currentLevel + 1))){
            globalData.setUpgrade(currentLevel + 1, UpgradeEnum.RecoveryFacility)
            setRecoveryFacilityMoney(currentLevel + 1)
            Toast.makeText(this, "Recovery facility upgraded to ${currentLevel+1} level", Toast.LENGTH_SHORT).show()
        }
    }

    private fun setRecoveryFacilityMoney(currentLevel: Int){
        val tv = findViewById<TextView>(R.id.recovery_facility_upgrade_money)
        val text = "${calculateUpgradeCost(currentLevel + 1)} $"
        tv.text = text
    }

    fun upgradeYouthAcademy(view: View){
        val globalData = (this.application as GlobalData)
        val currentLevel = globalData.getUpgrade(UpgradeEnum.YouthAcademy)
        if(globalData.removeMoney(calculateUpgradeCost(currentLevel + 1))){
            globalData.setUpgrade(currentLevel + 1, UpgradeEnum.YouthAcademy)
            setYouthAcademyMoney(currentLevel + 1)
            Toast.makeText(this, "Youth academy upgraded to ${currentLevel+1} level", Toast.LENGTH_SHORT).show()
        }
    }

    private fun setYouthAcademyMoney(currentLevel: Int){
        val tv = findViewById<TextView>(R.id.youth_academy_upgrade_money)
        val text = "${calculateUpgradeCost(currentLevel + 1)} $"
        tv.text = text
    }

    private fun calculateUpgradeCost(nextLevel: Int) : Long{
        var levelZero = 100.toLong()
        for(i in 1..nextLevel){
            levelZero *= 10
        }
        return levelZero
    }
}
