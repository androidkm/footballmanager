package com.example.androidkm.footballmanager.main_menu

import android.content.Intent
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.preference.PreferenceFragment
import android.support.v7.app.AlertDialog
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.example.androidkm.footballmanager.GlobalData
import com.example.androidkm.footballmanager.R
import com.example.androidkm.footballmanager.WelcomeScreenActivity
import com.example.androidkm.footballmanager.match.GameActivity
import com.example.androidkm.footballmanager.team.TeamManagementActivity
import com.example.androidkm.footballmanager.utils.TeamIconTranslator
import org.jetbrains.anko.doAsync

class MainMenuActivity : AppCompatActivity() {

    private lateinit var fragment: MyPreferenceFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_menu)

        fragment = MyPreferenceFragment()

        val globalData = (this.application as GlobalData)

        if(globalData.getMatchNumber() == 30) createNewSeason()

        val tv = findViewById<TextView>(R.id.main_menu_team_name)
        tv.text = globalData.getTeamName()

        val tv2 = findViewById<TextView>(R.id.main_menu_stadium_name)
        tv2.text = globalData.getStadiumName()

        val tv3 = findViewById<TextView>(R.id.main_menu_league_info)
        tv3.text = "${globalData.getMatchesLeft()} matches left to end of the league"

        val tv4 = findViewById<TextView>(R.id.main_menu_next_match_info)
        tv4.text = "Next match against : ${getNextRival()}"

        val iv = findViewById<ImageView>(R.id.main_menu_team_logo)
        iv.setImageResource(TeamIconTranslator.toResource(globalData.getTeamIcon()))

        prepareLastMatches()

        refreshMoney(globalData)
    }

    private fun getNextRival():String{
        val globalData = (this.application as GlobalData)

        val round = globalData.getCurrentRound()
        val teamId = globalData.getTeamId()

        for(r in round){

            if(r.first == teamId){
                return globalData.getTeamNameById(r.second)
            }

            if(r.second == teamId){
                return globalData.getTeamNameById(r.first)
            }
        }

        return ""
    }

    private fun prepareLastMatches(){
        var lastMatches = (this.application as GlobalData).getLastMatches()

        val tv1 = findViewById<TextView>(R.id.winLoseOne)
        tv1.text = lastMatches[0]

        val tv2 = findViewById<TextView>(R.id.winLoseTwo)
        tv2.text = lastMatches[1]

        val tv3 = findViewById<TextView>(R.id.winLoseThree)
        tv3.text = lastMatches[2]

        val tv4 = findViewById<TextView>(R.id.winLoseFour)
        tv4.text = lastMatches[3]

        val tv5 = findViewById<TextView>(R.id.winLoseFive)
        tv5.text = lastMatches[4]
    }

    override fun onResume() {
        super.onResume()

        val globalData = (this.application as GlobalData)
        refreshMoney(globalData)

    }

    private fun refreshMoney(globalData: GlobalData){
        var tv = findViewById<TextView>(R.id.main_menu_money)
        val text = "${globalData.getMoney()} $"
        tv.text = text
    }

    private fun createNewSeason(){

        val gd = (this.application as GlobalData)
        val teams = gd.getAllTeams().sortedByDescending { it.getTeamPoints() }

        doAsync {
            gd.createNewSeason()
        }

        val positsion = teams.indexOf(teams.find { it.id == gd.getTeamId() } ?: teams.first()) + 1

        var alert = AlertDialog.Builder(this)
        alert.setTitle("Congratulations")
        alert.setMessage("You ended up the league with ${positsion} position! The new season starts now.")

        alert.create().show()
    }

    fun teamStaffClick(view: View){
        startActivity(Intent(this@MainMenuActivity, TeamStaffActivity::class.java))
    }

    fun teamManagementClick(view: View){
        startActivity(Intent(this@MainMenuActivity, TeamManagementActivity::class.java))
    }

    fun facilitiesClick(view: View){
        startActivity(Intent(this@MainMenuActivity, FacilitiesActivity::class.java))
    }

    fun leagueTableClick(view: View){
        startActivity(Intent(this@MainMenuActivity, LeagueTableActivity::class.java))
    }

    fun nextMatchClick(view: View){
        startActivity(Intent(this@MainMenuActivity, GameActivity::class.java))
    }

    override fun onBackPressed() {
        val app = this.application
        doAsync {
            (app as GlobalData).saveAllData()
        }
        startActivity(Intent(this@MainMenuActivity, WelcomeScreenActivity::class.java))
    }

    class MyPreferenceFragment : PreferenceFragment(){

        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            addPreferencesFromResource(R.xml.settings)
        }

        override fun onActivityCreated(savedInstanceState: Bundle?) {
            view.setBackgroundColor(Color.WHITE)
            super.onActivityCreated(savedInstanceState)
        }

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_settings, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item != null && item.itemId == R.id.settings_button) {

            var fm = fragmentManager.beginTransaction()
            if(!fragment.isAdded) {
                fm.replace(android.R.id.content, fragment).commit()
            }
            else{
                fm.remove(fragment).commit()
            }

        }
        return super.onOptionsItemSelected(item)
    }

}
