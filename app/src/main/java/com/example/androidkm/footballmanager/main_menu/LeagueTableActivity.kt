package com.example.androidkm.footballmanager.main_menu

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.TextView
import com.example.androidkm.footballmanager.GlobalData
import com.example.androidkm.footballmanager.database.DbClub
import com.example.androidkm.footballmanager.database.FootballDatabase
import com.example.androidkm.footballmanager.R
import com.example.androidkm.footballmanager.database.DataBaseHelper
import com.example.androidkm.footballmanager.obj.LeagueTableEntry
import com.example.androidkm.footballmanager.obj.TeamBase
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread


class LeagueTableActivity : AppCompatActivity() {

    private var leaguetable: ArrayList<LeagueTableEntry> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_league_table)

        val myListView = findViewById<ListView>(R.id.leagueTableList)
        val adapter = LeagueTableAdapter(this, R.layout.league_entry_view, leaguetable)
        val header = layoutInflater.inflate(R.layout.league_entry_view, null)
        myListView.addHeaderView(header)
        myListView.adapter = adapter
        val gd = this.application as GlobalData

        doAsync{
            var teams = gd.getAllTeams()
            var entries = ArrayList<LeagueTableEntry>()

            for(item: TeamBase in teams){
                entries.add(LeagueTableEntry(0,item.name, item.getMatchesCount(), item.wins, item.draws, item.losses, item.getTeamPoints()))
            }

            var i = 0
            for(item2: LeagueTableEntry in entries.sortedByDescending { it.points }){
                leaguetable.add(LeagueTableEntry(++i, item2.teamName, item2.matches, item2.wins, item2.draws, item2.loses, item2.points))
            }

            uiThread {
                adapter.notifyDataSetChanged()
            }
        }
    }

    inner class LeagueTableAdapter(context: Context?, resource: Int, objects: ArrayList<LeagueTableEntry>) : ArrayAdapter<LeagueTableEntry>(context, resource, objects) {

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {

            var v = convertView
            if (v == null) {
                val vi: LayoutInflater = LayoutInflater.from(context)
                v = vi.inflate(R.layout.league_entry_view, null)
            }

            val p = getItem(position)
            if (p != null) {

                val tvPositon = v!!.findViewById<TextView>(R.id.teamTablePosition)
                val tvTeamName = v.findViewById<TextView>(R.id.teamNameTable)
                val tvMatches = v.findViewById<TextView>(R.id.matchAmountTable)
                val tvWins = v.findViewById<TextView>(R.id.winAmountTable)
                val tvDraws = v.findViewById<TextView>(R.id.drawAmountTable)
                val tvLoses = v.findViewById<TextView>(R.id.loseAmountTable)
                val tvPoints = v.findViewById<TextView>(R.id.pointsAmountTable)

                tvPositon.text = p.position.toString()
                tvTeamName.text = p.teamName
                tvMatches.text = p.matches.toString()
                tvWins.text = p.wins.toString()
                tvDraws.text = p.draws.toString()
                tvLoses.text = p.loses.toString()
                tvPoints.text = p.points.toString()
            }

            return v
        }

    }
}
