package com.example.androidkm.footballmanager.obj

import android.content.Context
import com.example.androidkm.footballmanager.database.DataBaseHelper
import com.example.androidkm.footballmanager.utils.*
import org.jetbrains.anko.doAsync

class MainTeam constructor(var name: String = "Default team name",
                           var stadiumName: String = "Default stadium name",
                           var money: Long = 250000,
                           var teamIcon: TeamIcon = TeamIcon.First,
                           var lastMatches: Array<String> = Array(5) { "" },
                           var players: ArrayList<PlayerBase> = ArrayList(),
                           var upgrades: TeamUpgrades = TeamUpgrades(),
                           var playstyle: Playstyle = Playstyle()) {

    var matchSquad: ArrayList<TacticsPosition> = ArrayList()
    var availableTraining = 2

    constructor(context: Context) : this() {
        //doAsync {
        var data = DataBaseHelper.getAllPlayers(context)

        for (i in 0..27) {
            players.add(data[i])
            DataBaseHelper.setPersonAsUsed(context, data[i].id)
        }

        for (i in arrayOf(0, 26, 27)) {
            players[i].position = Position.GK
            players[i].side = Side.C
            players[i].number = 1
            players[i].currentTrainng = TrainingType.GOALKEEPING
            players[i].skills = Skills.create(Position.GK)
        }

        var index = 1

        for (pos in Position.values()) {
            for (sid in Side.values()) {
                if (pos == Position.GK) {
                    continue
                } else {
                    players[index].position = pos
                    players[index].side = sid
                    index += 1
                    players[index].number = index
                }
            }
        }

        generateMatchSquad(Formation.formation4231())
        //}
    }

    fun generateMatchSquad(formation: Formation) {
        matchSquad = ArrayList()
        for (pos in formation.positions) {
            matchSquad.add(TacticsPosition(pos, null))
        }
    }
}
