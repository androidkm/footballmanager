package com.example.androidkm.footballmanager.team

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.example.androidkm.footballmanager.R
import com.example.androidkm.footballmanager.GlobalData
import com.example.androidkm.footballmanager.utils.Position
import com.example.androidkm.footballmanager.utils.TrainingType
import com.example.androidkm.footballmanager.utils.UpgradeEnum
import java.util.*
import android.widget.AdapterView.OnItemSelectedListener
import com.example.androidkm.footballmanager.obj.PlayerBase
import com.example.androidkm.footballmanager.utils.PauseStatus


class TeamTrainingActivity : AppCompatActivity() {

    lateinit var adapter: PlayerTransferAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_team_training)
        createList()
    }

    fun getSquad(): List<PlayerBase> {
        return (this.application as GlobalData).getSquad().filter { x -> x.status != PauseStatus.INJURY }
    }

    fun createList() {
        val list: ListView = findViewById(R.id.playerTrainingList)

        val squad = ArrayList<PlayerBase>()
        squad.addAll(getSquad())
        adapter = PlayerTransferAdapter(this, squad)
        val header = layoutInflater.inflate(R.layout.player_training_view_header, null)
        list.addHeaderView(header)

        if (!(this.application as GlobalData).isTrainingAvailable()) {
            val button: Button = findViewById(R.id.playerTrainingStarter)
            button.isEnabled = false
        }

        list.adapter = adapter
    }

    fun startTraining(view: View) {
        if ((this.application as GlobalData).isTrainingAvailable()) {
            for (i in (0..adapter.count - 1)) {
                completeTraining(adapter.getItem(i))
            }
            adapter.notifyDataSetInvalidated()
            (this.application as GlobalData).completeTraining()
            if (!(this.application as GlobalData).isTrainingAvailable()) {
                val button: Button = findViewById(R.id.playerTrainingStarter)
                button.isEnabled = false
            }
        } else {
            Toast.makeText(applicationContext, "No training left", Toast.LENGTH_SHORT).show()
        }
    }

    fun completeTraining(player: PlayerBase) {
        val r = Random()
        var trainerLevel = 1
        when (player.position) {
            Position.B -> trainerLevel += (applicationContext as GlobalData).getUpgrade(UpgradeEnum.DefendersTrainer)
            Position.F -> trainerLevel += (applicationContext as GlobalData).getUpgrade(UpgradeEnum.StrikersTrainer)
            Position.GK -> trainerLevel += (applicationContext as GlobalData).getUpgrade(UpgradeEnum.GoalkeepersTrainer)
            else -> trainerLevel += (applicationContext as GlobalData).getUpgrade(UpgradeEnum.MidfieldersTrainer)
        }
        var trainingResult: Double = r.nextInt(trainerLevel * 50) / 10.0
        if (player.position == Position.GK) {
            trainingResult /= 4
        }
        when (player.currentTrainng) {
            TrainingType.GOALKEEPING -> player.skills.goalkeeping += trainingResult
            TrainingType.DEFENCE -> player.skills.defence += trainingResult
            TrainingType.DRIBBLING -> player.skills.dribbling += trainingResult
            TrainingType.PASSING -> player.skills.passing += trainingResult
            TrainingType.SPEED -> player.skills.speed += trainingResult
            TrainingType.SHOOTING -> player.skills.shooting += trainingResult
            TrainingType.BALANCE -> {
                player.skills.speed += trainingResult / 5.0
                player.skills.defence += trainingResult / 5.0
                player.skills.dribbling += trainingResult / 5.0
                player.skills.shooting += trainingResult / 5.0
                player.skills.passing += trainingResult / 5.0
            }
        }
    }

    inner class PlayerTransferAdapter : BaseAdapter {

        private var list = ArrayList<PlayerBase>()
        private var context: Context? = null

        constructor(context: Context, notesList: ArrayList<PlayerBase>) : super() {
            this.list = notesList
            this.context = context
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {

            val view: View?
            val vh: ViewHolder

            if (convertView == null) {
                view = layoutInflater.inflate(R.layout.player_training_view, parent, false)
                vh = ViewHolder(view)
                view.tag = vh
                Log.i("JSA", "set Tag for ViewHolder, position: " + position)
            } else {
                view = convertView
                vh = view.tag as ViewHolder
            }

            val player = list[position]
            vh.playerPosition.text = player.getPosition()
            vh.playerFullName.text = player.getFullName()
            vh.playerOverall.text = player.getOverall().toString()
            vh.skillsShooting.text = player.skills.shooting.toInt().toString()
            vh.skillsDefence.text = player.skills.defence.toInt().toString()
            vh.skillsPassing.text = player.skills.passing.toInt().toString()
            vh.skillsDribbling.text = player.skills.dribbling.toInt().toString()
            vh.skillsSpeed.text = player.skills.speed.toInt().toString()
            vh.skillsGoalkeeping.text = player.skills.goalkeeping.toInt().toString()

            createDropDown(view!!, list[position])

            return view
        }

        fun createDropDown(view: View, playerTraining: PlayerBase) {
            val spinner: Spinner = view.findViewById(R.id.training_options)
            val adapter = ArrayAdapter.createFromResource(view.context,
                    R.array.training_types, android.R.layout.simple_spinner_item)
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner.adapter = adapter
            spinner.setSelection(
                    when (playerTraining.currentTrainng) {
                        TrainingType.BALANCE -> 0
                        TrainingType.SPEED -> 1
                        TrainingType.PASSING -> 2
                        TrainingType.SHOOTING -> 3
                        TrainingType.DRIBBLING -> 4
                        TrainingType.DEFENCE -> 5
                        TrainingType.GOALKEEPING -> 6
                    }
            )

            spinner.onItemSelectedListener = object : OnItemSelectedListener {
                override fun onItemSelected(parentView: AdapterView<*>, selectedItemView: View, position: Int, id: Long) {
                    playerTraining.currentTrainng = when (position) {
                        0 -> TrainingType.BALANCE
                        1 -> TrainingType.SPEED
                        2 -> TrainingType.PASSING
                        3 -> TrainingType.SHOOTING
                        4 -> TrainingType.DRIBBLING
                        5 -> TrainingType.DEFENCE
                        6 -> TrainingType.GOALKEEPING
                        else -> TrainingType.BALANCE
                    }
                }

                override fun onNothingSelected(parentView: AdapterView<*>) {
                    playerTraining.currentTrainng = TrainingType.BALANCE
                }

            }
        }

        override fun getItem(position: Int): PlayerBase {
            return list[position]
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getCount(): Int {
            return list.size
        }
    }

    private class ViewHolder(view: View?) {
        val playerPosition: TextView
        val playerFullName: TextView
        val playerOverall: TextView
        val skillsShooting: TextView
        val skillsPassing: TextView
        val skillsDribbling: TextView
        val skillsSpeed: TextView
        val skillsDefence: TextView
        val skillsGoalkeeping: TextView

        init {
            playerPosition = view?.findViewById<TextView>(R.id.playerPositionView) as TextView
            playerFullName = view?.findViewById<TextView>(R.id.playerNameView) as TextView
            playerOverall = view?.findViewById<TextView>(R.id.playerOverallView) as TextView
            skillsShooting = view?.findViewById<TextView>(R.id.playerSkillsShooting) as TextView
            skillsPassing = view?.findViewById<TextView>(R.id.playerSkillsPassing) as TextView
            skillsDefence = view?.findViewById<TextView>(R.id.playerSkillsDefence) as TextView
            skillsDribbling = view?.findViewById<TextView>(R.id.playerSkillsDribbling) as TextView
            skillsSpeed = view?.findViewById<TextView>(R.id.playerSkillsSpeed) as TextView
            skillsGoalkeeping = view?.findViewById<TextView>(R.id.playerSkillsGoalkeeping) as TextView
        }
    }
}
