package com.example.androidkm.footballmanager.team

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ListView
import android.widget.TextView
import android.widget.Toast
import com.example.androidkm.footballmanager.GlobalData
import com.example.androidkm.footballmanager.R
import com.example.androidkm.footballmanager.database.DataBaseHelper
import com.example.androidkm.footballmanager.obj.PlayerBase
import com.example.androidkm.footballmanager.utils.UpgradeEnum
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.toast
import javax.microedition.khronos.opengles.GL

class TeamTransferActivity : AppCompatActivity() {

    lateinit var ownPlayersAdapter: PlayerTransferAdapter
    lateinit var otherPlayersAdapter: PlayerTransferAdapter

    var playerToSell: PlayerBase? = null
    var playerToBuy: PlayerBase? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_team_transfer)
        createTemplate()
        createTransfterMarkt()
    }

    fun acceptExchange(view: View) {
        // TODO czy aby na pewno chcesz dokonać takiej wymiany - wyświetlić to
        if (playerToSell != null && playerToBuy != null) {
            showDialog(view, playerToBuy!!, playerToSell!!)
        }
    }

    private fun showDialog(view: View, playerToBuy: PlayerBase, playerToSell: PlayerBase) {
        // Late initialize an alert dialog object
        lateinit var dialog: AlertDialog

        val cost = playerToBuy.getValue() - playerToSell.getValue()
        // Initialize a new instance of alert dialog builder object
        val builder = AlertDialog.Builder(this)

        // Set a title for alert dialog
        builder.setTitle("Are you sure?")

        // Set a message for alert dialog
        builder.setMessage("You account balance change by: -" + cost.toString())

        // On click listener for dialog buttons
        val dialogClickListener = DialogInterface.OnClickListener { _, which ->
            when (which) {
                DialogInterface.BUTTON_POSITIVE -> {

                    if ((application as GlobalData).removeMoney(cost)) {
                        ownPlayersAdapter.removeItem(playerToSell)
                        otherPlayersAdapter.removeItem(playerToBuy)

                        ownPlayersAdapter.addItem(playerToBuy)
                        otherPlayersAdapter.addItem(playerToSell)

                        val foundPlayer = (application as GlobalData).getSquad().find { x -> x.id == playerToSell.id }
                        (application as GlobalData).getSquad().remove(foundPlayer)
                        (application as GlobalData).getSquad().add(playerToBuy)

                        resetExchange(view)
                    }
                }
                DialogInterface.BUTTON_NEGATIVE -> toast("Negative/No button clicked.")
            }
        }

        // Set the alert dialog positive/yes button
        builder.setPositiveButton("YES", dialogClickListener)

        // Set the alert dialog negative/no button
        builder.setNegativeButton("NO", dialogClickListener)

        // Initialize the AlertDialog using builder object
        dialog = builder.create()

        // Finally, display the alert dialog
        dialog.show()
    }

    fun Context.toast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    fun resetExchange(view: View) {
        playerToBuy = null
        playerToSell = null
    }

    private fun createTemplate() {
        val list: ListView = findViewById(R.id.playerToSellList)

        val squad = (this.application as GlobalData).getSquad()

        var playersList = ArrayList<PlayerBase>()

        for (player in squad) {
            playersList.add(player)
        }

        ownPlayersAdapter = PlayerTransferAdapter(this, playersList)

        list.adapter = ownPlayersAdapter
        list.choiceMode = ListView.CHOICE_MODE_SINGLE
        list.setItemChecked(1, true)

        val header = layoutInflater.inflate(R.layout.player_transfer_view, null)
        list.addHeaderView(header)

        list.setOnItemClickListener { parent, view, position, id ->
            playerToSell = ownPlayersAdapter.getItem(position - 1)
        }
    }

    private fun createTransfterMarkt() {

        val list: ListView = findViewById(R.id.playerToBuyList)
        val context = this
        doAsync {
            val freePlayers = DataBaseHelper.getAllFreePlayers(applicationContext)

            var playersList = ArrayList<PlayerBase>()

            for (i: Int in 0..10) {
                var headhunterLevel = (applicationContext as GlobalData).getUpgrade(UpgradeEnum.Headhunter)
                var matchNumber = ((context.application as GlobalData).getMatchNumber() + ((context.application as GlobalData).getSeasonNumber() * 30))
                freePlayers[i].skills.change(matchNumber, 1.0)
                freePlayers[i].skills.change(headhunterLevel)
                playersList.add(freePlayers[i])
            }
            runOnUiThread {
                otherPlayersAdapter = PlayerTransferAdapter(context, playersList);

                list.adapter = otherPlayersAdapter
                list.choiceMode = ListView.CHOICE_MODE_SINGLE
                list.setItemChecked(1, true)

                val header = layoutInflater.inflate(R.layout.player_transfer_view, null)
                list.addHeaderView(header)

                list.setOnItemClickListener { parent, view, position, id ->
                    playerToBuy = otherPlayersAdapter.getItem(position - 1)
                }
            }

        }

    }


    inner class PlayerTransferAdapter : BaseAdapter {

        private var list = ArrayList<PlayerBase>()
        private var context: Context? = null

        constructor(context: Context, list: ArrayList<PlayerBase>) : super() {
            this.list = list
            this.context = context
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {

            val view: View?
            val vh: ViewHolder

            if (convertView == null) {
                view = layoutInflater.inflate(R.layout.player_transfer_view, parent, false)
                vh = ViewHolder(view)
                view.tag = vh
                Log.i("JSA", "set Tag for ViewHolder, position: " + position)
            } else {
                view = convertView
                vh = view.tag as ViewHolder
            }

            val player = list[position];
            vh.playerPosition.text = player.getPosition()
            vh.playerFullName.text = player.getFullName()
            vh.playerValue.text = player.getValueString()
            vh.playerOverall.text = player.getOverall().toString()

            return view
        }

        override fun getItem(position: Int): PlayerBase {
            return list[position]
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getCount(): Int {
            return list.size
        }

        fun removeItem(player: PlayerBase) {
            list.remove(player)
            notifyDataSetChanged()
        }

        fun addItem(player: PlayerBase) {
            list.add(player)
            notifyDataSetChanged()
        }
    }

    private class ViewHolder(view: View?) {
        val playerPosition: TextView
        val playerFullName: TextView
        val playerValue: TextView
        val playerOverall: TextView

        init {
            playerPosition = view?.findViewById<TextView>(R.id.playerPosition) as TextView
            playerFullName = view?.findViewById<TextView>(R.id.playerName) as TextView
            playerValue = view?.findViewById<TextView>(R.id.playerValue) as TextView
            playerOverall = view?.findViewById<TextView>(R.id.playerOverall) as TextView
        }
    }
}
