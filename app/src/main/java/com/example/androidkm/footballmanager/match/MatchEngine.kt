package com.example.androidkm.footballmanager.match

import android.content.Context
import android.preference.PreferenceManager
import android.view.View
import android.widget.Button
import android.widget.TextView
import com.example.androidkm.footballmanager.GlobalData
import com.example.androidkm.footballmanager.R
import com.example.androidkm.footballmanager.obj.PlayerBase
import com.example.androidkm.footballmanager.obj.TacticsPosition
import com.example.androidkm.footballmanager.utils.PauseStatus
import com.example.androidkm.footballmanager.utils.Position
import com.example.androidkm.footballmanager.utils.RandomGetter
import com.example.androidkm.footballmanager.utils.UpgradeEnum
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.util.*

/**
 * Created by Karol on 2018-05-23.
 */
class MatchEngine(val adapter: GameActivity.MatchLogAdapter, val context: Context, val appContext: Context,
                  val view: View, val teams: Array<ArrayList<TacticsPosition>>) {

    var delay: Long = 500
    var currentMinute = 0

    private var teamWithBall = 0
    private var playerWithBall: TacticsPosition? = null
    private var playersCounter = arrayOf(11, 11)
    private var doctorLevel = (appContext as GlobalData).getUpgrade(UpgradeEnum.TeamDoctor)
    private var recoveryLevel = (appContext as GlobalData).getUpgrade(UpgradeEnum.RecoveryFacility)

    val results = arrayOf(0, 0)
    private val resultTextViews = arrayOf(view.findViewById<TextView>(R.id.homeTeamScore), view.findViewById<TextView>(R.id.awayTeamScore))

    fun run() {
        val stringValue = PreferenceManager.getDefaultSharedPreferences(context).getString("actionDelaySetting", "500")
        delay = stringValue.toLongOrNull() ?: 500
        doAsync {
            for (i in 1..90) {
                currentMinute = i
                if (playersCounter[0] < 6) {
                    results[0] = 0
                    results[1] = Math.max(results[1], 3)
                    break
                }
                if (playersCounter[1] < 6) {
                    results[1] = 0
                    results[0] = Math.max(results[0], 3)
                    break
                }
                if (i == 46) {
                    Thread.sleep(10 * delay)
                    teamWithBall = 1
                    playerWithBall = null
                }
                Thread.sleep(delay)
                uiThread {

                    view.findViewById<TextView>(R.id.matchTime).text = i.toString() + "'"
                }
                if (playerWithBall == null) {
                    playerWithBall = RandomGetter.randomPlayer(teams[teamWithBall])
                }
                val playerDef = RandomGetter.randomPlayer(teams[(teamWithBall + 1) % 2])

                var offAction = chooseOffensiveAction()
                var defAction = chooseDefensiveAction()

                if (defAction == DefensiveActions.FOUL) {
                    val gk = teams[(teamWithBall + 1) % 2].find { x -> x.fullPosition.pos == Position.GK }
                    val playerOff = RandomGetter.randomPlayer(teams[teamWithBall])
                    uiThread {
                        val foulEffect = foul(playerOff, playerDef, gk!!, adapter, i)
                        if (foulEffect == null) {
                            adapter.insert(MatchLogEntry.notFaulLog(i, context), 0)
                        } else if (foulEffect) {
                            val entry = MatchLogEntry.createOffensiveLog(i, OffensiveActions.SHOOT, playerWithBall!!.player!!.getFullName(), context)
                            if (entry != null) {
                                adapter.insert(entry, 0)
                            }
                            results[teamWithBall]++
                            resultTextViews[teamWithBall].text = results[teamWithBall].toString()
                        } else {
                            adapter.insert(MatchLogEntry.createSaveLog(i, context), 0)
                        }
                    }
                    continue
                }
                if (defAction == DefensiveActions.FAILURE && offAction == OffensiveActions.FAILURE) {
                    continue
                }

                if (defAction != DefensiveActions.FAILURE && offAction != OffensiveActions.FAILURE) {
                    val offActionPassed = isOffActionPassed(offAction, defAction, playerDef, playerWithBall!!)
                    if (offActionPassed) {
                        defAction = DefensiveActions.FAILURE
                    } else {
                        offAction = OffensiveActions.FAILURE
                    }
                }

                if (defAction == DefensiveActions.FAILURE) {
                    when (offAction) {
                        OffensiveActions.DRIBBLING -> {
                        }
                        OffensiveActions.SPRINT -> {
                        }
                        OffensiveActions.SHOOT -> {
                            val gk = teams[(teamWithBall + 1) % 2].find { x -> x.fullPosition.pos == Position.GK }
                            val goal = shoot(playerWithBall!!, gk!!)
                            if (goal) {
                                uiThread {
                                    val entry = MatchLogEntry.createOffensiveLog(i, OffensiveActions.SHOOT, playerWithBall!!.player!!.getFullName(), context)
                                    if (entry != null) {
                                        adapter.insert(entry, 0)
                                    }
                                    results[teamWithBall]++
                                    resultTextViews[teamWithBall].text = results[teamWithBall].toString()
                                }
                            }
                        }
                        OffensiveActions.PASS -> {
                            playerWithBall = RandomGetter.randomPlayer(teams[teamWithBall])
                        }
                        OffensiveActions.FAILURE -> {
                        }
                    }
                    continue
                }
                if (offAction == OffensiveActions.FAILURE) {
                    when (defAction) {
                        DefensiveActions.BLOCK -> {
                            playerWithBall = RandomGetter.randomPlayer(teams[teamWithBall])
                        }
                        DefensiveActions.TACKLE -> {
                            playerWithBall = playerDef
                            teamWithBall = ((teamWithBall + 1) % 2)
                        }
                        DefensiveActions.COUNTERATTACK -> {
                            teamWithBall = ((teamWithBall + 1) % 2)
                            playerWithBall = RandomGetter.randomPlayer(teams[teamWithBall])
                        }
                        else -> {
                        }
                    }
                    continue
                }
            }

            uiThread {
                view.findViewById<Button>(R.id.skip_match_button).text = (context.getString(R.string.end_match_string))
            }
        }
    }

    fun chooseOffensiveAction(): OffensiveActions {
        return RandomGetter.randomEnum(OffensiveActions::class.java)
    }

    fun chooseDefensiveAction(): DefensiveActions {
        return RandomGetter.randomEnum(DefensiveActions::class.java)
    }

    fun foul(playerOff: TacticsPosition, playerDef: TacticsPosition, gk: TacticsPosition, adapter: GameActivity.MatchLogAdapter, minute: Int): Boolean? {
        val r = Random()
        val value = r.nextInt(4)
        var entry: MatchLogEntry? = null
        when (value) {
            1 -> {
                val cardValue = r.nextInt(8)
                when (cardValue) {
                    1 -> {
                        playerDef.player!!.status = PauseStatus.RED_CARD
                        playerDef.player!!.pauseMatch = 3
                        entry = MatchLogEntry.redCardLog(minute, context, playerDef.player!!.getFullName())
                        playerDef.player = null
                        playersCounter[(teamWithBall + 1) % 2]--
                    }
                    else -> {
                        playerDef.player!!.yellowCards += 1
                        if (playerDef.player!!.status == PauseStatus.YELLOW_CARD) {
                            playerDef.player!!.status = PauseStatus.RED_CARD
                            playerDef.player!!.pauseMatch = 2
                            entry = MatchLogEntry.secondYellowCardLog(minute, context, playerDef.player!!.getFullName())
                            playerDef.player!!.yellowCards -= 1
                            playerDef.player = null
                            playersCounter[(teamWithBall + 1) % 2]--
                        } else {
                            playerDef.player!!.status = PauseStatus.YELLOW_CARD
                            entry = MatchLogEntry.yellowCardLog(minute, context, playerDef.player!!.getFullName())
                        }
                    }
                }
            }
            2 -> {
                val injuryValue = r.nextInt(doctorLevel + 1)
                when (injuryValue) {
                    0 -> {
                        playerOff.player!!.status = PauseStatus.INJURY
                        playerOff.player!!.pauseMatch = r.nextInt(10 - recoveryLevel) + 1
                        entry = MatchLogEntry.injuryLog(minute, context, playerDef.player!!.getFullName())
                        playerOff.player = (appContext as GlobalData).getReservePlayers().firstOrNull { x -> x.position != Position.GK }
                    }
                }
            }
            3 -> if (r.nextInt(3) == 1) {
                return null
            }
            else -> {
            }
        }
        if (entry != null) {
            adapter.insert(entry, 0)
        }
        return shoot(playerOff, gk)
    }

    fun isOffActionPassed(offAction: OffensiveActions, defAction: DefensiveActions, offPos: TacticsPosition, defPos: TacticsPosition): Boolean {
        val r = Random()
        if (offPos.player == null) return false
        val offPlayer = offPos.player!!
        if (defPos.player == null) return true
        val defPlayer = defPos.player!!
        var offSkills = r.nextInt(30) + 4 * playersCounter[teamWithBall]
        var defSkills = r.nextInt(50) + 5 * playersCounter[(teamWithBall + 1) % 2]
        if (offPlayer.position == offPos.fullPosition.pos) {
            offSkills += 10
        }
        if (offPlayer.side == offPos.fullPosition.side) {
            offSkills += 10
        }
        if (defPlayer.position == defPos.fullPosition.pos) {
            defSkills += 10
        }
        if (defPlayer.side == defPos.fullPosition.side) {
            defSkills += 10
        }
        when (offAction) {
            OffensiveActions.SHOOT -> offSkills += offPlayer.skills.shooting.toInt()
            OffensiveActions.PASS -> offSkills += offPlayer.skills.passing.toInt()
            OffensiveActions.SPRINT -> offSkills += offPlayer.skills.speed.toInt()
            OffensiveActions.DRIBBLING -> offSkills += offPlayer.skills.dribbling.toInt()
            OffensiveActions.FAILURE -> {
            }
        }
        when (defAction) {
            DefensiveActions.COUNTERATTACK -> defSkills += defPlayer.skills.speed.toInt()
            DefensiveActions.BLOCK -> defSkills += defPlayer.skills.defence.toInt()
            DefensiveActions.TACKLE -> defSkills += defPlayer.skills.defence.toInt()
            else -> {

            }
        }
        return offSkills > defSkills
    }

    fun shoot(striker: TacticsPosition, gk: TacticsPosition): Boolean {
        val strikerPlayer = striker.player!!
        val gkPlayer = gk.player!!
        val gkSkills = Random().nextInt(100) + gkPlayer.getOverall()
        val strikerSkills = Random().nextInt(50) + (strikerPlayer.getOverall() / 2.0) + (strikerPlayer.skills.shooting / 2.0)
        return strikerSkills > gkSkills
    }
}