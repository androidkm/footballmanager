package com.example.androidkm.footballmanager.main_menu

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import android.widget.Toast
import com.example.androidkm.footballmanager.GlobalData
import com.example.androidkm.footballmanager.R
import com.example.androidkm.footballmanager.utils.UpgradeEnum

class TeamStaffActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_team_staff)

        val globalData = (this.application as GlobalData)
        setGoalkeepersTrainerMoney(globalData.getUpgrade(UpgradeEnum.GoalkeepersTrainer))
        setDefendersTrainerMoney(globalData.getUpgrade(UpgradeEnum.DefendersTrainer))
        setMidfieldersTrainerMoney(globalData.getUpgrade(UpgradeEnum.MidfieldersTrainer))
        setStrikersTrainerMoney(globalData.getUpgrade(UpgradeEnum.StrikersTrainer))
        setTeamDoctorMoney(globalData.getUpgrade(UpgradeEnum.TeamDoctor))
        setHeadhunterMoney(globalData.getUpgrade(UpgradeEnum.Headhunter))
    }

    fun upgradeGoalkeepersTrainer(view:View){
        val globalData = (this.application as GlobalData)
        val currentLevel = globalData.getUpgrade(UpgradeEnum.GoalkeepersTrainer)
        if(globalData.removeMoney(calculateUpgradeCost(currentLevel + 1))){
            globalData.setUpgrade(currentLevel + 1, UpgradeEnum.GoalkeepersTrainer)
            setGoalkeepersTrainerMoney(currentLevel + 1)
            Toast.makeText(this, "Goalkeepers trainer upgraded to ${currentLevel+1} level", Toast.LENGTH_SHORT).show()
        }
    }

    private fun setGoalkeepersTrainerMoney(currentLevel: Int){
        val tv = findViewById<TextView>(R.id.goalkeeper_trainer_money)
        val text = "${calculateUpgradeCost(currentLevel + 1)} $"
        tv.text = text
    }

    fun upgradeDefendersTrainer(view:View){
        val globalData = (this.application as GlobalData)
        val currentLevel = globalData.getUpgrade(UpgradeEnum.DefendersTrainer)
        if(globalData.removeMoney(calculateUpgradeCost(currentLevel + 1))){
            globalData.setUpgrade(currentLevel + 1, UpgradeEnum.DefendersTrainer)
            setDefendersTrainerMoney(currentLevel + 1)
            Toast.makeText(this, "Defenders trainer upgraded to ${currentLevel+1} level", Toast.LENGTH_SHORT).show()
        }
    }

    private fun setDefendersTrainerMoney(currentLevel: Int){
        val tv = findViewById<TextView>(R.id.defenders_trainer_money)
        val text = "${calculateUpgradeCost(currentLevel + 1)} $"
        tv.text = text
    }

    fun upgradeMidfieldersTrainer(view:View){
        val globalData = (this.application as GlobalData)
        val currentLevel = globalData.getUpgrade(UpgradeEnum.MidfieldersTrainer)
        if(globalData.removeMoney(calculateUpgradeCost(currentLevel + 1))){
            globalData.setUpgrade(currentLevel + 1, UpgradeEnum.MidfieldersTrainer)
            setMidfieldersTrainerMoney(currentLevel + 1)
            Toast.makeText(this, "Midfielders trainer upgraded to ${currentLevel+1} level", Toast.LENGTH_SHORT).show()
        }
    }

    private fun setMidfieldersTrainerMoney(currentLevel: Int){
        val tv = findViewById<TextView>(R.id.midfielders_trainer_money)
        val text = "${calculateUpgradeCost(currentLevel + 1)} $"
        tv.text = text
    }

    fun upgradeStrikersTrainer(view:View){
        val globalData = (this.application as GlobalData)
        val currentLevel = globalData.getUpgrade(UpgradeEnum.StrikersTrainer)
        if(globalData.removeMoney(calculateUpgradeCost(currentLevel + 1))){
            globalData.setUpgrade(currentLevel + 1, UpgradeEnum.StrikersTrainer)
            setStrikersTrainerMoney(currentLevel + 1)
            Toast.makeText(this, "Strikers trainer upgraded to ${currentLevel+1} level", Toast.LENGTH_SHORT).show()
        }
    }

    private fun setStrikersTrainerMoney(currentLevel: Int){
        val tv = findViewById<TextView>(R.id.strikers_trainer_money)
        val text = "${calculateUpgradeCost(currentLevel + 1)} $"
        tv.text = text
    }

    fun upgradeTeamDoctor(view:View){
        val globalData = (this.application as GlobalData)
        val currentLevel = globalData.getUpgrade(UpgradeEnum.TeamDoctor)
        if(globalData.removeMoney(calculateUpgradeCost(currentLevel + 1))){
            globalData.setUpgrade(currentLevel + 1, UpgradeEnum.TeamDoctor)
            setTeamDoctorMoney(currentLevel + 1)
            Toast.makeText(this, "Team doctor upgraded to ${currentLevel+1} level", Toast.LENGTH_SHORT).show()
        }
    }

    private fun setTeamDoctorMoney(currentLevel: Int){
        val tv = findViewById<TextView>(R.id.team_doctor_money)
        val text = "${calculateUpgradeCost(currentLevel + 1)} $"
        tv.text = text
    }

    fun upgradeHeadhunter(view:View){
        val globalData = (this.application as GlobalData)
        val currentLevel = globalData.getUpgrade(UpgradeEnum.Headhunter)
        if(globalData.removeMoney(calculateUpgradeCost(currentLevel + 1))){
            globalData.setUpgrade(currentLevel + 1, UpgradeEnum.Headhunter)
            setHeadhunterMoney(currentLevel + 1)
            Toast.makeText(this, "Headhunter upgraded to ${currentLevel+1} level", Toast.LENGTH_SHORT).show()
        }
    }

    private fun setHeadhunterMoney(currentLevel: Int){
        val tv = findViewById<TextView>(R.id.headhunter_money)
        val text = "${calculateUpgradeCost(currentLevel + 1)} $"
        tv.text = text
    }

    private fun calculateUpgradeCost(nextLevel: Int) : Long{
        var levelZero = 100.toLong()
        for(i in 1..nextLevel){
            levelZero *= 10
        }
        return levelZero
    }
}
