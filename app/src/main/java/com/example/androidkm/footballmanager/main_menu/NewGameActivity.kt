package com.example.androidkm.footballmanager.main_menu

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import com.example.androidkm.footballmanager.GlobalData
import com.example.androidkm.footballmanager.R
import com.example.androidkm.footballmanager.utils.TeamIcon
import com.example.androidkm.footballmanager.utils.TeamIconTranslator
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread


class NewGameActivity : FragmentActivity() {

    private val NUM_PAGES = 6

    private var mPager: ViewPager? = null

    private var mPagerAdapter: PagerAdapter? = null

    private var currentPosition: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_game)

        // Instantiate a ViewPager and a PagerAdapter.
        mPager = findViewById<View>(R.id.emblem_slider) as ViewPager
        mPagerAdapter = ScreenSlidePagerAdapter(supportFragmentManager)
        mPager!!.adapter = mPagerAdapter
    }

    private inner class ScreenSlidePagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {

        override fun getItem(position: Int): Fragment {
            var pageFragment = ScreenSlidePageFragment()
            var args = Bundle()
            args.putInt("position", position)
            pageFragment.arguments = args
            return pageFragment
        }

        override fun getCount(): Int {
            return NUM_PAGES
        }
    }

    fun applyClick(view: View) {
        val globalData = this.application as GlobalData

        doAsync {
            globalData.createNewData()

            val editText = findViewById<EditText>(R.id.newTeamNameEditText)
            globalData.setNewTeamName(editText.text.toString())

            val editText2 = findViewById<EditText>(R.id.newStadiumNameEditText)
            globalData.setNewStadiumName(editText2.text.toString())

            if (mPager == null) {
                globalData.setNewTeamIcon(TeamIcon.First)
            } else {
                when (mPager!!.currentItem) {
                    0 -> globalData.setNewTeamIcon(TeamIcon.First)
                    1 -> globalData.setNewTeamIcon(TeamIcon.Second)
                    2 -> globalData.setNewTeamIcon(TeamIcon.Third)
                    3 -> globalData.setNewTeamIcon(TeamIcon.Fourth)
                    4 -> globalData.setNewTeamIcon(TeamIcon.Fifth)
                    5 -> globalData.setNewTeamIcon(TeamIcon.Sixth)
                }
            }

            globalData.saveAllData()

            uiThread {
                startActivity(Intent(this@NewGameActivity, MainMenuActivity::class.java))
            }
        }

    }

}

class ScreenSlidePageFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {


        var v = inflater.inflate(
                R.layout.fragment_emblem_slider, container, false) as ViewGroup

        var pos = arguments!!.get("position")
        var image: ImageView? = v.findViewById(R.id.emblem_image)

        if (pos != null) {
            when (pos) {
                0 -> image!!.setImageResource(TeamIconTranslator.toResource(TeamIcon.First))
                1 -> image!!.setImageResource(TeamIconTranslator.toResource(TeamIcon.Second))
                2 -> image!!.setImageResource(TeamIconTranslator.toResource(TeamIcon.Third))
                3 -> image!!.setImageResource(TeamIconTranslator.toResource(TeamIcon.Fourth))
                4 -> image!!.setImageResource(TeamIconTranslator.toResource(TeamIcon.Fifth))
                5 -> image!!.setImageResource(TeamIconTranslator.toResource(TeamIcon.Sixth))
            }
        }
        return v
    }

}


