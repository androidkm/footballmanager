package com.example.androidkm.footballmanager.team

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.ViewGroup
import com.example.androidkm.footballmanager.R
import android.widget.TextView
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.ListView
import com.example.androidkm.footballmanager.GlobalData
import com.example.androidkm.footballmanager.obj.PlayerBase
import com.example.androidkm.footballmanager.utils.PauseStatus


class TeamStatusActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_team_status)
        createTemplate()
    }

    fun createTemplate() {
        val list: ListView = findViewById(R.id.playerStatusList)

        var playersList = (this.application as GlobalData).getSquad()

        val header = layoutInflater.inflate(R.layout.player_staus_view, null)
        list.addHeaderView(header)

        val adapter = PlayerStatusAdapter(this, playersList);

        list.adapter = adapter
    }

    inner class PlayerStatusAdapter : BaseAdapter {

        private var list = ArrayList<PlayerBase>()
        private var context: Context? = null

        constructor(context: Context, notesList: ArrayList<PlayerBase>) : super() {
            this.list = notesList
            this.context = context
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {

            val view: View?
            val vh: ViewHolder

            if (convertView == null) {
                view = layoutInflater.inflate(R.layout.player_staus_view, parent, false)
                vh = ViewHolder(view)
                view.tag = vh
                Log.i("JSA", "set Tag for ViewHolder, position: " + position)
            } else {
                view = convertView
                vh = view.tag as ViewHolder
            }

            val player = list[position];
            vh.playerPosition.text = player.getPosition()
            vh.playerFullName.text = player.getFullName()
            if (player.status == PauseStatus.RED_CARD || player.status == PauseStatus.YELLOW_CARD) {
                vh.playerStatus.setImageResource(R.drawable.football_card_with_cross_mark);
            } else if (player.yellowCards >= 2) {
                vh.playerStatus.setImageResource(R.drawable.yellow_cards)
            } else if (player.status == PauseStatus.INJURY) {
                vh.playerStatus.setImageResource(R.drawable.player_injury)
            } else {
                vh.playerStatus.setImageResource(R.drawable.ready_to_play)
            }

            vh.playerPauseMatch.text = player.pauseMatch.toString();

            return view
        }

        override fun getItem(position: Int): Any {
            return list[position]
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getCount(): Int {
            return list.size
        }
    }

    private class ViewHolder(view: View?) {
        val playerPosition: TextView
        val playerFullName: TextView
        val playerPauseMatch: TextView
        val playerStatus: ImageView

        init {
            playerPosition = view?.findViewById<TextView>(R.id.playerPositionView) as TextView
            playerFullName = view?.findViewById<TextView>(R.id.playerNameView) as TextView
            playerPauseMatch = view?.findViewById<TextView>(R.id.pauseMatchAmount) as TextView
            playerStatus = view?.findViewById<ImageView>(R.id.pauseCause) as ImageView
        }

        //  if you target API 26, you should change to:
//        init {
//            this.tvTitle = view?.findViewById<TextView>(R.id.tvTitle) as TextView
//            this.tvContent = view?.findViewById<TextView>(R.id.tvContent) as TextView
//        }
    }
}

