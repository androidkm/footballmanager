package com.example.androidkm.footballmanager.utils

/**
 * Created by Karol on 2018-04-08.
 */
enum class PauseStatus {
    RED_CARD, INJURY, READY_TO_PLAY, YELLOW_CARD
}