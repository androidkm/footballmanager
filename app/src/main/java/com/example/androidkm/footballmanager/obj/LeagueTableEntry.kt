package com.example.androidkm.footballmanager.obj

class LeagueTableEntry(val position: Int, val teamName : String, val matches: Int, val wins: Int, val draws: Int, val loses: Int, val points: Int) {
}