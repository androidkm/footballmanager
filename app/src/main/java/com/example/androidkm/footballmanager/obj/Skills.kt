package com.example.androidkm.footballmanager.obj

import com.example.androidkm.footballmanager.utils.Position
import java.io.Serializable
import java.util.*

/**
 * Created by Karol on 2018-05-31.
 */
class Skills : Serializable {
    var dribbling = 10.0
    var shooting = 10.0
    var speed = 10.0
    var passing = 10.0
    var defence = 10.0
    var goalkeeping = 10.0

    constructor() {
        val random = Random()
        dribbling += random.nextInt(70)
        shooting += random.nextInt(70)
        speed += random.nextInt(70)
        passing += random.nextInt(70)
        defence += random.nextInt(70)
        goalkeeping += random.nextInt(20)
    }

    fun gkOverall(): Double {
        val sum = dribbling + shooting + speed + passing + defence + (20 * goalkeeping)
        return (sum / 25.0)
    }

    fun fieldOverall(): Double {
        val sum = dribbling + shooting + speed + passing + defence
        return (sum / 5.0)
    }

    fun createGoalKepper(): Skills {
        val skills = Skills()
        skills.shooting /= 5
        skills.passing /= 5
        skills.defence /= 5
        skills.dribbling /= 5
        skills.speed /= 5
        skills.goalkeeping = 20.0
        val random = Random()
        skills.goalkeeping += random.nextInt(60)
        return skills
    }

    companion object {

        fun create(position: Position?): Skills {
            val skills = Skills()
            if (position != null && position == Position.GK) {
                return skills.createGoalKepper()
            }
            return skills
        }
    }

    constructor(dribble: Double, shoot: Double, speed: Double, pass: Double, def: Double, gk: Double) {
        this.dribbling = dribble
        this.shooting = shoot
        this.speed = speed
        this.passing = pass
        this.defence = def
        this.goalkeeping = gk
    }

    fun change(level: Int) {
        change(level, 10.0)
    }

    fun change(level: Int, base: Double) {
        this.defence += level * base
        this.speed += level * base
        this.passing += level * base
        this.shooting += level * base
        this.dribbling += level * base
        this.goalkeeping += level * base
        if (this.defence < 0) this.defence = 0.0
        if (this.dribbling < 0) this.dribbling = 0.0
        if (this.goalkeeping < 0) this.goalkeeping = 0.0
        if (this.passing < 0) this.passing = 0.0
        if (this.shooting < 0) this.shooting = 0.0
        if (this.speed < 0) this.speed = 0.0
    }
}