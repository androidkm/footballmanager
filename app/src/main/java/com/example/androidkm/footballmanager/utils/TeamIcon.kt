package com.example.androidkm.footballmanager.utils

import com.example.androidkm.footballmanager.R

enum class TeamIcon {
    First,
    Second,
    Third,
    Fourth,
    Fifth,
    Sixth
}

class TeamIconTranslator{
    companion object {

        fun toResource(teamIcon: TeamIcon): Int{
            return when(teamIcon){
                TeamIcon.First -> R.drawable.team_one_icon
                TeamIcon.Second -> R.drawable.team_two_icon
                TeamIcon.Third -> R.drawable.team_three_icon
                TeamIcon.Fourth -> R.drawable.team_four_icon
                TeamIcon.Fifth -> R.drawable.team_five_icon
                TeamIcon.Sixth -> R.drawable.strikers_icon
            }
        }

        fun toTeamIcon(id: Int): TeamIcon{
            return when(id){
                R.drawable.team_one_icon -> TeamIcon.First
                R.drawable.team_two_icon -> TeamIcon.Second
                R.drawable.team_three_icon -> TeamIcon.Third
                R.drawable.team_four_icon -> TeamIcon.Fourth
                R.drawable.team_five_icon -> TeamIcon.Fifth
                R.drawable.strikers_icon -> TeamIcon.Sixth
                else -> { TeamIcon.First }
            }
        }

    }
}

