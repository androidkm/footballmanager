package com.example.androidkm.footballmanager

import android.app.Application
import android.util.Log
import android.widget.Toast
import com.example.androidkm.footballmanager.obj.*
import com.example.androidkm.footballmanager.utils.Formation
import com.example.androidkm.footballmanager.utils.MatchResult
import com.example.androidkm.footballmanager.utils.TeamIcon
import com.example.androidkm.footballmanager.utils.UpgradeEnum
import com.google.gson.Gson
import org.jetbrains.anko.defaultSharedPreferences

class GlobalData : Application() {

    private var mainTeam: MainTeam = MainTeam()
    private var leagueInfo: LeagueInfo = LeagueInfo()

    fun getTeamId(): Int {
        return 16
    }

    fun getSquad(): ArrayList<PlayerBase> {
        return mainTeam.players
    }

    fun getReservePlayers(): ArrayList<PlayerBase> {
        val reservePlayers = ArrayList<PlayerBase>()
        reservePlayers.addAll(mainTeam.players)
        for (pos in getMatchSquad()) {
            if (pos.player == null) continue
            reservePlayers.removeAll { x -> x.id == pos.player!!.id && x.name == pos.player!!.name }
        }
        reservePlayers.removeAll { x -> !(x.isAbleToPlay()) }
        return reservePlayers
    }

    fun getMatchSquad(): ArrayList<TacticsPosition> {
        return mainTeam.matchSquad
    }

    fun clearMatchSquad() {
        for (pos in mainTeam.matchSquad) {
            if (pos.player == null) continue
            val player = mainTeam.players.find { x -> x.id == pos.player!!.id && x.name == pos.player!!.name }
            if (player == null || !(player.isAbleToPlay())) {
                pos.player = null
            }
        }
    }

    fun completeMatchSquad() {
        val reservePlayers = getReservePlayers()
        for (pos in mainTeam.matchSquad) {
            if (pos.player == null) {
                pos.player = reservePlayers.first()
                reservePlayers.remove(pos.player!!)
            }
        }
    }

    fun setMatchSquad(formation: Formation) {
        mainTeam.generateMatchSquad(formation)
    }

    fun setNewTeamName(newName: String) {
        mainTeam.name = newName
        leagueInfo.teams.add(TeamBase(getTeamId(), newName))
    }

    fun getTeamName(): String {
        return mainTeam.name
    }

    fun setNewStadiumName(newName: String) {
        mainTeam.stadiumName = newName
    }

    fun getStadiumName(): String {
        return mainTeam.stadiumName
    }

    fun setNewTeamIcon(teamIcon: TeamIcon) {
        mainTeam.teamIcon = teamIcon
    }

    fun getTeamIcon(): TeamIcon {
        return mainTeam.teamIcon
    }

    fun setPlaystyle(playstyle: Playstyle) {
        mainTeam.playstyle = playstyle
    }

    fun getPlaystyle(): Playstyle {
        return mainTeam.playstyle
    }

    fun getMatchesLeft(): Int {
        return 2 * (leagueInfo.teams.count() - 1) - leagueInfo.currentRoundIndex
    }

    fun getMatchNumber(): Int {
        return leagueInfo.currentRoundIndex
    }

    fun getSeasonNumber(): Int{
        return leagueInfo.currentSeason
    }

    fun getTeamNameById(id: Int): String {
        if (id < 1 || id > 16) return "Wrong index"

        for (team in leagueInfo.teams) {
            if (team.id == id) return team.name
        }

        return "Team do not exist"
    }

    fun getLastMatches(): Array<String> {
        return mainTeam.lastMatches
    }

    fun addLastMatch(result: String) {
        for (i in 0..3) {
            mainTeam.lastMatches[i] = mainTeam.lastMatches[i + 1]
        }
        mainTeam.lastMatches[4] = result
    }

    fun removeMoney(value: Long): Boolean {
        if (mainTeam.money < value) {
            Toast.makeText(applicationContext, "Not enough money", Toast.LENGTH_SHORT).show()
            return false
        } else {
            mainTeam.money -= value
            return true
        }
    }

    fun setMatchResult(teamId: Int, result: MatchResult) {
        for (team in leagueInfo.teams) {
            if (team.id == teamId) {
                when (result) {
                    MatchResult.Win -> team.wins++
                    MatchResult.Draw -> team.draws++
                    MatchResult.Lose -> team.losses++
                }
            }
        }
    }

    fun getUpgrade(upgrade: UpgradeEnum): Int {
        when (upgrade) {

            UpgradeEnum.GoalkeepersTrainer -> return mainTeam.upgrades.gTrainerLevel
            UpgradeEnum.DefendersTrainer -> return mainTeam.upgrades.dTrainerLevel
            UpgradeEnum.MidfieldersTrainer -> return mainTeam.upgrades.mTrainerLevel
            UpgradeEnum.StrikersTrainer -> return mainTeam.upgrades.sTrainerLevel
            UpgradeEnum.TeamDoctor -> return mainTeam.upgrades.doctorLevel
            UpgradeEnum.Headhunter -> return mainTeam.upgrades.headhunterLevel
            UpgradeEnum.StadiumSeats -> return mainTeam.upgrades.stadiumLevel
            UpgradeEnum.StadiumAttractions -> return mainTeam.upgrades.attractionsLevel
            UpgradeEnum.RecoveryFacility -> return mainTeam.upgrades.recoveryLevel
            UpgradeEnum.YouthAcademy -> return mainTeam.upgrades.academyLevel
        }
    }

    fun setUpgrade(level: Int, upgrade: UpgradeEnum) {
        when (upgrade) {

            UpgradeEnum.GoalkeepersTrainer -> mainTeam.upgrades.gTrainerLevel = level
            UpgradeEnum.DefendersTrainer -> mainTeam.upgrades.dTrainerLevel = level
            UpgradeEnum.MidfieldersTrainer -> mainTeam.upgrades.mTrainerLevel = level
            UpgradeEnum.StrikersTrainer -> mainTeam.upgrades.sTrainerLevel = level
            UpgradeEnum.TeamDoctor -> mainTeam.upgrades.doctorLevel = level
            UpgradeEnum.Headhunter -> mainTeam.upgrades.headhunterLevel = level
            UpgradeEnum.StadiumSeats -> mainTeam.upgrades.stadiumLevel = level
            UpgradeEnum.StadiumAttractions -> mainTeam.upgrades.attractionsLevel = level
            UpgradeEnum.RecoveryFacility -> mainTeam.upgrades.recoveryLevel = level
            UpgradeEnum.YouthAcademy -> mainTeam.upgrades.academyLevel = level
        }
    }

    fun addMoney(value: Long) {
        mainTeam.money += value
    }

    fun getMoney(): Long {
        return mainTeam.money
    }

    fun isTrainingAvailable(): Boolean {
        return mainTeam.availableTraining > 0
    }

    fun newTrainings() {
        mainTeam.availableTraining = 2
    }

    fun completeTraining() {
        mainTeam.availableTraining--
    }


    fun getCurrentRound(): ArrayList<Pair<Int, Int>> {
        return leagueInfo.rounds[leagueInfo.currentRoundIndex]
    }

    fun increaseCurrentRound() {
        leagueInfo.currentRoundIndex++
    }

    fun getAllTeams(): ArrayList<TeamBase> {
        return leagueInfo.teams
    }

    fun createNewSeason(){
        var currentSeason = leagueInfo.currentSeason
        leagueInfo = LeagueInfo(applicationContext)
        leagueInfo.teams.add(TeamBase(getTeamId(), mainTeam.name))
        leagueInfo.currentSeason = currentSeason + 1
    }

    fun saveAllData() {
        var prefsEditor = defaultSharedPreferences.edit()
        var json = Gson().toJson(mainTeam)
        var json2 = Gson().toJson(leagueInfo)
        prefsEditor.putString("wholeData", json)
        prefsEditor.putString("leagueInfo", json2)
        prefsEditor.apply()
    }

    fun createNewData() {
        mainTeam = MainTeam(applicationContext)
        leagueInfo = LeagueInfo(applicationContext)
    }

    fun loadAllData() {
        var json = defaultSharedPreferences.getString("wholeData", "")
        mainTeam = Gson().fromJson(json, MainTeam::class.java)
        var json2 = defaultSharedPreferences.getString("leagueInfo", "")
        leagueInfo = Gson().fromJson(json2, LeagueInfo::class.java)
    }

}