package com.example.androidkm.footballmanager.team

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ListView
import android.widget.TextView
import com.example.androidkm.footballmanager.GlobalData
import com.example.androidkm.footballmanager.R
import com.example.androidkm.footballmanager.obj.PlayerBase
import com.example.androidkm.footballmanager.obj.TacticsPosition
import com.example.androidkm.footballmanager.utils.FullPosition
import com.example.androidkm.footballmanager.utils.Position

class PlayerSelectActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_player_select)
        val b = intent.extras
        val pos = b.getSerializable("POS") as FullPosition
        val player = b.getSerializable("PLAYER") as PlayerBase?
        (findViewById<TextView>(R.id.player_selection_position)).text = pos.toString()
        val view = (findViewById<View>(R.id.player_selection_current_player));
        if (player != null) {
            view.findViewById<TextView>(R.id.playerPositionView).text = player.getPosition()
            view.findViewById<TextView>(R.id.playerNameView).text = player.getFullName()
            view.findViewById<TextView>(R.id.playerOverallView).text = player.getOverall().toString()
            view.findViewById<TextView>(R.id.playerConditionView).text = player.condition.toString()
        } else {
            view.findViewById<TextView>(R.id.playerNameView).text = "EMPTY"
        }

          val squad = (this.application as GlobalData).getReservePlayers()
//        if (player != null) {
//            squad.add(player)
//        }
        val listView = findViewById<ListView>(R.id.player_selection_other_players)

        listView.adapter = PlayerAdapter(applicationContext, squad)

        listView.setOnItemClickListener { parent, view, position, id ->
            val intent = Intent()
            val player = (listView.adapter.getItem(position) as PlayerBase)
            intent.putExtra("PLAYER", player)
            //squad.remove(player)
            setResult(Activity.RESULT_OK, intent)
            finish()
        }
    }

    inner class PlayerAdapter : BaseAdapter {

        private var list = ArrayList<PlayerBase>()
        private var context: Context? = null

        constructor(context: Context, notesList: ArrayList<PlayerBase>) : super() {
            this.list = notesList
            this.context = context
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {

            val view: View?
            val vh: ViewHolder

            if (convertView == null) {
                view = layoutInflater.inflate(R.layout.player_tactics_view, parent, false)
                vh = ViewHolder(view)
                view.tag = vh
                Log.i("JSA", "set Tag for ViewHolder, position: " + position)
            } else {
                view = convertView
                vh = view.tag as ViewHolder
            }

            val player = list[position]
            vh.playerPosition.text = player.getPosition()
            vh.playerFullName.text = player.getFullName()
            vh.playerOverall.text = player.getOverall().toString()
            vh.playerCondition.text = player.condition.toString()
            return view
        }

        override fun getItem(position: Int): Any {
            return list[position]
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getCount(): Int {
            return list.size
        }
    }

    private class ViewHolder(view: View?) {
        val playerPosition: TextView
        val playerFullName: TextView
        val playerOverall: TextView
        val playerCondition: TextView

        init {
            playerPosition = view?.findViewById<TextView>(R.id.playerPositionView) as TextView
            playerFullName = view?.findViewById<TextView>(R.id.playerNameView) as TextView
            playerOverall = view?.findViewById<TextView>(R.id.playerOverallView) as TextView
            playerCondition = view?.findViewById<TextView>(R.id.playerConditionView) as TextView
        }
    }
}
