package com.example.androidkm.footballmanager.obj

class TeamBase(var id: Int,
               var name: String,
               var wins: Int = 0,
               var draws: Int = 0,
               var losses: Int = 0) {

    fun getTeamPoints(): Int {
        return (wins * 3) + draws
    }

    fun getMatchesCount(): Int{
        return wins + draws + losses
    }

}
