package com.example.androidkm.footballmanager.match

import android.content.Context
import com.example.androidkm.footballmanager.R
import com.example.androidkm.footballmanager.obj.PlayerBase

class MatchLogEntry(val minute: Int, val text: String) {

    companion object {
        fun createOffensiveLog(minute: Int, off: OffensiveActions, playerName: String, context: Context): MatchLogEntry? {
            when (off) {
                OffensiveActions.SHOOT -> return MatchLogEntry(minute, (context.getString(R.string.log_player)) +
                        playerName + context.getString(R.string.log_goal))
            }
            return null
        }

        fun createDefensiveLog(minute: Int, def: DefensiveActions, playerName: String, context: Context): MatchLogEntry? {
            when (def) {
                DefensiveActions.FOUL -> return MatchLogEntry(minute, (context.getString(R.string.log_player)) + playerName +
                        (context.getString(R.string.log_foul)))
                DefensiveActions.COUNTERATTACK -> return MatchLogEntry(minute, (context.getString(R.string.log_player)) + playerName +
                        (context.getString(R.string.log_counter)))
            }
            return null
        }

        fun notFaulLog(minute: Int, context: Context): MatchLogEntry {
            return MatchLogEntry(minute, context.getString(R.string.log_not_foul))
        }

        fun createSaveLog(minute: Int, context: Context): MatchLogEntry {
            return MatchLogEntry(minute, context.getString(R.string.log_save))
        }

        fun yellowCardLog(minute: Int, context: Context, playerName: String): MatchLogEntry {
            return MatchLogEntry(minute, (context.getString(R.string.log_player)) + playerName
                    + context.getString(R.string.log_yellow_card))
        }

        fun redCardLog(minute: Int, context: Context, playerName: String): MatchLogEntry {
            return MatchLogEntry(minute, (context.getString(R.string.log_player)) + playerName
                    + context.getString(R.string.log_red_card))
        }

        fun secondYellowCardLog(minute: Int, context: Context, playerName: String): MatchLogEntry {
            return MatchLogEntry(minute, (context.getString(R.string.log_player)) + playerName
                    + context.getString(R.string.log_yellow_card) + context.getString(R.string.log_second_yellow_card))
        }

        fun injuryLog(minute: Int, context: Context, playerName: String): MatchLogEntry {
            return MatchLogEntry(minute, (context.getString(R.string.log_player)) + playerName
                    + context.getString(R.string.log_injury))
        }
    }
}