package com.example.androidkm.footballmanager.database

import android.content.Context
import com.example.androidkm.footballmanager.obj.LeagueTableEntry
import com.example.androidkm.footballmanager.obj.PlayerBase
import com.example.androidkm.footballmanager.obj.TeamBase
import com.example.androidkm.footballmanager.utils.Position
import com.example.androidkm.footballmanager.utils.RandomGetter
import com.example.androidkm.footballmanager.utils.Side
import java.util.*

class DataBaseHelper {
    companion object {


        fun getAllPlayers(context: Context): ArrayList<PlayerBase> {
            val data = FootballDatabase.getInstance(context).personDao().getAllPeople()
            var players = ArrayList<PlayerBase>()

            for (item: DbPerson in data) {
                players.add(PlayerBase(item.uid, item.uid.toInt(), Position.GK, Side.C, item.firstName, item.lastName, 100))
            }
            return players
        }

        fun getAllFreePlayers(context: Context): ArrayList<PlayerBase> {
            val data = FootballDatabase.getInstance(context).personDao().getAllFreePeople()
            var players = ArrayList<PlayerBase>()
            val r = Random()
            for (item: DbPerson in data) {
                players.add(PlayerBase(r.nextLong(), item.uid.toInt(), RandomGetter.randomEnum(Position::class.java),
                        RandomGetter.randomEnum(Side::class.java), item.firstName, item.lastName, 100))
            }
            return players
        }

        fun getAllTeams(context: Context): ArrayList<TeamBase> {
            val data = FootballDatabase.getInstance(context).clubDao().getAllClubs()
            var teams = ArrayList<TeamBase>()

            for (item: DbClub in data) {
                teams.add(TeamBase(item.uid.toInt(), item.name))
            }
            return teams
        }

        fun setPersonAsUsed(context: Context, playerId: Long) {
            FootballDatabase.getInstance(context).personDao().setPersonAsUsed(playerId)
        }

    }
}