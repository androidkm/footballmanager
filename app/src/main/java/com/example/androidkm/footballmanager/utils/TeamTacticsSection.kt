package com.example.androidkm.footballmanager.utils

enum class TeamTacticsSection {
    SQUAD,
    FORMATION,
    PLAYSTYLE
}