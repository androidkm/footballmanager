package com.example.androidkm.footballmanager

import com.example.androidkm.footballmanager.utils.Formation
import org.junit.Assert.*
import org.junit.Test

/**
 * Created by Karol on 2018-06-06.
 */
class FormationsTests {
    @Test
    fun autocompleteTest() {
        val formation = Formation()
        formation.autoComplete()
        assertEquals(formation.hasMaxPlayers(), true)
    }
}