package com.example.androidkm.footballmanager

import com.example.androidkm.footballmanager.obj.PlayerBase
import com.example.androidkm.footballmanager.utils.PauseStatus
import com.example.androidkm.footballmanager.utils.Position
import com.example.androidkm.footballmanager.utils.Side
import org.junit.Assert.*
import org.junit.Test

/**
 * Created by Karol on 2018-06-06.
 */
class PlayerTests {
    val player = PlayerBase(1, 1, Position.M, Side.C, "Test", "Test", 100)
    @Test
    fun injuryTest() {
        player.status = PauseStatus.INJURY
        assertEquals(false, player.isAbleToPlay())
    }

    @Test
    fun readyToPlayTest() {
        player.status = PauseStatus.READY_TO_PLAY
        assertEquals(true, player.isAbleToPlay())
    }

    @Test
    fun redCardTest() {
        player.status = PauseStatus.RED_CARD
        assertEquals(false, player.isAbleToPlay())
    }

    @Test
    fun skillsChangeTest() {
        val ovr = player.getOverall()
        player.skills.change(1, 1.0)
        assertEquals(ovr + 1, player.getOverall(), 0.001)
    }
}